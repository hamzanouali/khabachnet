<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('questions', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user_id');
           $table->string('title');
           $table->text('topic');
           $table->integer('category_id')->unsigned();
           $table->integer('likes')->default(0);
           $table->enum('is_closed',[1,0])->default(0);
           $table->enum('is_answered',[1,0])->default(0);
           $table->integer('comments')->default(0);
           $table->timestamps();

           $table->foreign('category_id')->references('id')->on('categories');
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('questions');
     }
}
