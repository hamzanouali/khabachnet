<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_exams', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->longtext('question');
          $table->longtext('answer');
          $table->string('timer');
          $table->string('category');
          $table->text('tags');
          $table->text('background');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_exams');
    }
}
