<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('language');
            $table->string('title');
            $table->longtext('question');
            $table->longtext('answer');
            $table->string('timer');
            $table->string('category');
            $table->text('tags');
            $table->text('background');
            $table->enum('with_prediction',[1,0])->default(0);
            $table->longtext('predicted_answers')->nullable();
            $table->text('predicted_messages')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
