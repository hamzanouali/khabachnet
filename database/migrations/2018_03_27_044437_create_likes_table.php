<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            // target could be post (question) or comment
            $table->integer('target_id')->unsigned();
            // 0: dislike, 1: like
            $table->string('action');
            $table->string('target_type');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            // $table->foreign('target_id')->references('id')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
