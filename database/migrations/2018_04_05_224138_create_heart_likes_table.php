<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeartLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heart_likes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('maxim_id')->unsigned();
            $table->string('action')->default('null');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('maxim_id')->references('id')->on('maxims');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heart_likes');
    }
}
