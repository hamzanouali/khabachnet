<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // $i = 0;
      // for ($i=0; $i < 100; $i++) {
      //   \DB::table('questions')->insert([
      //     'user_id' => 1,
      //     'title' => str_random(10),
      //     'category' => str_random(5),
      //     'topic' => str_random(10),
      //     'is_closed' => 1,
      //     'is_answered' => 1,
      //     'comments' => str_random(10),
      //   ]);
      // }

      for ($i=104; $i < 150 ; $i++) {
        \DB::table('tag_question')->insert([
          'tag_id' => 3,
          'question_id' => $i
        ]);
      }

      for ($i=104; $i < 120 ; $i++) {
        \DB::table('tag_question')->insert([
          'tag_id' => 2,
          'question_id' => $i
        ]);
      }

      for ($i=104; $i < 130 ; $i++) {
        \DB::table('tag_question')->insert([
          'tag_id' => 1,
          'question_id' => $i
        ]);
      }

    }
}
