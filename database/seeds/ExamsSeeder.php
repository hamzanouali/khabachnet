<?php

use Illuminate\Database\Seeder;

class ExamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        for($i ; $i< 300 ; $i++){
  
          \DB::table('exams')->insert([
            'language' => 'ar',
            'title' => str_random(30),
            'question' => str_random(100),
            'answer' => str_random(100),
            'timer' => 50,
            'category' => str_random(10),
            'tags' => str_random(20),
            'background' => '{"card_back":"background-image: url(images/backs/back_2.png)","background":"\n              background-image: linear-gradient(-60deg, #16a085de 0%, #f4d03f85 100%);\n            "}'
          ]);

          \DB::table('exams')->insert([
            'language' => 'en',
            'title' => str_random(30),
            'question' => str_random(100),
            'answer' => str_random(100),
            'timer' => 50,
            'category' => str_random(10),
            'tags' => str_random(20),
            'background' => '{"card_back":"background-image: url(images/backs/back_2.png)","background":"\n              background-image: linear-gradient(-60deg, #16a085de 0%, #f4d03f85 100%);\n            "}'
          ]);
        }
    }
}
