<?php

Route::get('/sitemap.xml', 'SitemapController@index');

Route::group(['middleware'=>['auth','valide_user']],function(){

  Route::get('/change-password',function(){
    return view('auth.passwords.changepassword');
  });

  Route::post('/changepassword','ChangePasswordController@update')->name('changepassword');

  // notification

  Route::get('/not-readed-notifications/{skip}','NotificationController@indexNotReaded')->where('skip','[0-9]+');

  Route::post('/deleteAllNotifications','NotificationController@deleteAChunk');

  Route::post('/markAsReadSomeNotifications','NotificationController@markAsReadChunck');

  /*-------------
  START : LIKE DISKLIKE ROUTES
  -------------*/
  Route::post('/like-dislike','LikeController@create');

  Route::post('/save-user-info','SkillController@updateUserInfo');

  Route::post('/block-user/{id}',function($id){
    \DB::table('users')->where('id',$id)->update(['is_active'=>2]);
    return 'true';
  })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/unblock-user/{id}',function($id){
      \DB::table('users')->where('id',$id)->update(['is_active'=>1]);
      return 'true';
    })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/make-a-writer/{id}',function($id){
    \DB::table('users')->where('id',$id)->update(['role'=>2]);
    return 'true';
  })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/make-a-user/{id}',function($id){
      \DB::table('users')->where('id',$id)->update(['role'=>0]);
      return 'true';
    })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/post_a_question','QuestionController@create');

  Route::post('/update_a_question/{id}','QuestionController@update')->where('id','[0-9]+');

  Route::post('/update_a_comment/{id}','CommentController@update')->where('id','[0-9]+');

  Route::post('/post_a_comment','CommentController@create');

  Route::post('/createExam','ExamController@create')->middleware('role:1');

  Route::post('/saveTmpExam','ExamController@updateTmp')->middleware('role:1');

  Route::get('/get_tmp_exams_existance','ExamController@getTmpExamsExistance')->middleware('role:1');

  Route::get('/get_tmp_exams_for_update/{count}',function($count){
    $result = \App\Exam::latest()->with('user')->skip($count)->take(15)->get(['id','title']);
    return json_encode($result);
  })->middleware('role:1');

  Route::get('/get_exams_for_update/{count}',function($count){
    $result = \App\Exam::latest()->with('user')->skip($count)->take(15)->get(['id','title']);
    return json_encode($result);
  })->middleware('role:1');

  Route::get('/get_tmp_exam/{id}',function($id){
    $result = \App\Exam::whereId($id)->with('user')->get();
    return json_encode($result);
  })->middleware('role:1');

  Route::post('/remove_tmp_exam','ExamController@destroyTmp')->middleware('role:1');

  Route::post('/remove_exam','ExamController@destroy')->middleware('role:1');

  Route::post('/update_exam','ExamController@update')->middleware('role:1');

  Route::post('save_tag','TagsController@create')->middleware('role:1');

  Route::post('removeTag','TagsController@destroy')->middleware('role:1');

  Route::post('save_category','CategoriesController@create')->middleware('role:1');

  Route::post('/save_followed_categories','CategoriesController@saveForUser');

  Route::post('removecategory','CategoriesController@destroy')->middleware('role:1');

  Route::get('/getRememberList',function(){
      $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->get(['id','title']);
      return json_encode($data);
  });

  Route::post('/addToRememberList','RememberListController@create');

  Route::get('/deleteRememberList',function(){
      $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->delete();
      return json_encode($data);
  });

  Route::post('/save-skill','SkillController@create');

  // website routes
  Route::post('/update-instructions-of-use','WebsiteController@updateInstructions')->middleware('role:1');

  Route::post('/update-posting-rules','WebsiteController@updatePostingRules')->middleware('role:1');

  Route::post('/update-ads','WebsiteController@updateAds')->middleware('role:1');

  Route::get('/get_website_info',function(){
    $coll = collect();
    $coll = $coll->concat(collect(\DB::table('users')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('questions')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('exams')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('maxims')->get()->count()));
    return json_encode($coll);
  })->middleware('role:1');

  Route::post('/switch_user_role/{id}/{role}',function($id,$role){
    \DB::table('users')->where('id',$id)->update([
      'role' => $role
    ]);
  })->where(['id'=>'[0-9]+','role'=>'[0-1]'])->middleware('role:1');

  Route::post('/close-target','QuestionController@closeTarget');

  Route::post('/open-target','QuestionController@openTarget');

  Route::post('/remove_question/{id}',function($id){
    $obj = \App\Question::with('user')->where('id','=',$id)->first();
    if(
      $obj->user->id != \Auth::id() &&
      \Auth::user()['role'] != 1
    ){
      return false;
    }

    if($obj->comments != 0 && $obj->likes != 0 && \Auth::user()['role'] != 1) {
      return '401-forbidden';
    }

    \DB::table('questions')->where('id',$id)->delete();
    \DB::table('comments')->where('question_id',$id)->delete();
    \DB::table('likes')->where('target_type','question')->where('target_id',$id)->delete();

    return 'true';
  })->where('id','[0-9]+');

  Route::post('/remove_comment/{id}',function($id){
    $obj = \App\Comment::with('user')->where('id','=',$id)->first();
    if(
      $obj->user->id != \Auth::id() &&
      \Auth::user()['role'] != 1
    ){
      return false;
    }

    if($obj->replies != 0 && $obj->likes != 0 && \Auth::user()['role'] != 1) {
      return '401-forbidden';
    }

    \DB::table('comments')->where('id',$id)->delete();
    \DB::table('likes')->where('target_type','comment')->where('target_id',$id)->delete();

    return 'true';
  })->where('id','[0-9]+');

  Route::post('/save_maxim','MaximController@create');

  Route::post('/unlike_maxim/{id}',function($id){

    // in case that the user already unliked his like
    if(\DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->where('action','false')->get()->count())
      abort(422,'oops');

    // unlike the unliked
    if(\DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->get()->count())
    {
      // unlike just if the user have liked before, it's not like and dislike system
      \DB::table('maxims')->where('id',$id)->decrement('likes');
      return \DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->update(['action'=>'false']);
    }

  })->where('id','[0-9]+');

  Route::post('/like_maxim/{id}',function($id){

    // in case that the user already liked this maxim
    if(\DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->where('action','true')->get()->count())
      abort(422,'oops');

    \DB::table('maxims')->where('id',$id)->increment('likes');
    // unlike the unliked
    if(\DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->get()->count())
    {
      \DB::table('heart_likes')->where('user_id',\Auth::id())->where('maxim_id',$id)->update(['action'=>'true']);
    }else{
      \DB::table('heart_likes')->insert([
        'user_id' => \Auth::id(),
        'maxim_id' => $id,
        'action' => 'true'
      ]);
    }

  })->where('id','[0-9]+');

});

Route::get('/get_random_maxim',function(){
  // return json_encode(\App\Maximwith('user')->inRandomOrder()->first());
  $coll = collect();
  $maxim = \App\Maxim::with('user')->inRandomOrder()->first(['topic','id','user_id','likes']);
  $coll = $coll->concat(collect($maxim));

  if(\DB::table('heart_likes')->where('maxim_id',$maxim->id)->where('user_id',\Auth::id())->get()->count())
    $coll = $coll->concat(collect(\DB::table('heart_likes')->where('maxim_id',$maxim->id)->where('user_id',\Auth::id())->first(['action'])));
  return $coll;

});

Route::get('/get-instructions-of-use',function(){
  return json_encode(\DB::table('website')->first(['instructions_of_use']));
});

Route::get('/get-posting-rules',function(){
  return json_encode(\DB::table('website')->first(['posting_rules']));
});
//
Route::get('/get_questions',function(){
  $q = \App\Question::with('tag')->get();
  echo "<pre>";
  // $q = $question->with('tag')->get();
  foreach ($q as $v1) {

    echo "<b>title:<b> ".$v1->title."\n";
    echo "tags: ";
    foreach ($v1->tag as $value) {
      print_r($value->name.", ");
    }
    echo "<hr>";

  }
  echo "</pre>";
});

//search for questions
Route::get('/search_for_questions/{title}/{skip}/{take}',function($title,$skip,$take){

  // empty string
  if(empty($title) || $title == ' ') return '';

  // convert title to lower case
  // $title = strtolower($title);

  // number of words
  $num_of_words = count(explode(" ",$title));

  // last position of white space
  $last_white_space = strrpos($title," ");

  if($last_white_space == 0 && !empty($title)) {
    return \App\Question::where('title','LIKE','%'.$title.'%')->skip($skip)->take($take)->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //

  // loop through title words
  while($last_white_space != 0) {
    $title = substr($title,0,$last_white_space);
    // search for questions by title
    // then push the result to the collection
    $res = \App\Question::where('title','LIKE','%'.$title.'%')->skip($skip)->take($take)->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($title," ");
  }

})->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','take' => '[0-9]+']);

//search for exams
Route::get('/search_for_exams/{title}/{skip}/{take}',function($title,$skip,$take){

  // empty string
  if(empty($title) || $title == ' ') return '';

  // convert title to lower case
  // $title = strtolower($title);

  // number of words
  $num_of_words = count(explode(" ",$title));

  // last position of white space
  $last_white_space = strrpos($title," ");

  if($last_white_space == 0 && !empty($title)) {
    return \App\Exam::where('title','LIKE','%'.$title.'%')->with('category')->skip($skip)->take($take)->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //
  // print_r(substr($title,0,$number_of_words));

  // loop through title words
  while($last_white_space != 0) {
    $title = substr($title,0,$last_white_space);
    // search for questions by title
    // then push the result to the collection
    $res = \App\Exam::where('title','LIKE','%'.$title.'%')->skip($skip)->take($take)->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($title," ");
  }

})->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','take' => '[0-9]+']);

//search for users
Route::get('/search_for_users/{name}/{skip}/{take}',function($name,$skip,$take){

  // empty string
  if(empty($name) || $name == ' ') return '';

  // convert title to lower case
  // $name = strtolower($name);

  // number of words
  $num_of_words = count(explode(" ",$name));

  // last position of white space
  $last_white_space = strrpos($name," ");

  if($last_white_space == 0 && !empty($name)) {
    return \App\User::where('name','LIKE','%'.$name.'%')->skip($skip)->take($take)->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //
  // print_r(substr($title,0,$number_of_words));

  // loop through title words
  while($last_white_space != 0) {
    $name = substr($name,0,$last_white_space);
    // search for questions by title
    // then push the result to the collection
    $res = \App\User::where('name','LIKE','%'.$name.'%')->skip($skip)->take($take)->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($name," ");
  }

})->where(['name' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','take' => '[0-9]+']);

Route::get('/get-users/{skip}/{order?}',function($skip,$order = null){
  return \DB::table('users')
    ->latest()
    ->skip($skip)
    ->take(15)
    ->get();
})->where('skip','[0-9]+');


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home',function(){
  return redirect('/#/');
});

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : HOME PAGE ROUTES
  -------------*/

  Route::get('/recentExams/{count}/{take}',function($count,$take){
      $data = \App\Exam::whereLanguage($_COOKIE['language'])
        ->with('category')
        ->latest()
        ->skip($count)
        ->take($take)
        ->get(['id','title','category_id','background']);
      return json_encode($data);
  })->where(['count'=>'[0-9]+','take'=>'[0-9]+']);

  Route::get('/category/{category_id}/{count}',function($category_id,$count){
      $data = \DB::table('exams')->latest()->whereCategory($category_id)->skip($count)->take(3)->get(['id','title','category_id','background']);
      return json_encode($data);
  })->where(['category_id' => '[0-9]+','count' => '[0-9]+']);

  Route::get('/exams_where_category/{category_id}/{count}/{take}',function($category_id,$count,$take = 10){

      $data = \App\Exam::latest()->with('category')->where('category_id',$category_id)->take($take)->skip($count)->get(['id','title','category_id','background']);
      return json_encode($data);

  })->where(['category'=>'[0-9]+','count'=>'[0-9]+','[0-9]+']);


  /*|
    |
    |
    |
    |
    |
    |
    |
    |
    -------------
    START : AUTH ROUTES
    -------------*/

  Auth::routes();

  Route::get('/isAuth',function(){
      $obj;
      if(\Auth::check()){
        $user = \DB::table('user_info')->where('user_id',\Auth::user()['id'])->first();
        $obj = '{'.
        ' "id" : '.\Auth::user()['id'].','.
        ' "name" : "'.\Auth::user()['name'].'",'.
        ' "is_active" : "'.\Auth::user()['is_active'].'",'.
        ' "role" : "'.\Auth::user()['role'].'",'.
        ' "created_at" : "'.\Auth::user()['created_at'].'",'.
        ' "points" : '.$user->likes.','.
        ' "comments" : '.$user->comments.','.
        ' "questions" : '.$user->questions.','.
        ' "reputation" : '.($user->comments*0.25 + $user->questions * 0.5 + $user->likes).''.
        '}';
      }else{
        $obj = 'false';
      }
      return $obj;
  });

  Route::get('/logout',function(){
    \Auth::logout();
    return redirect('/login');
    // return \Auth::check() ? 'true' : 'false';
  });

  Route::get('/ger-user-info/{id}',function($id){
    $collection = collect();
    $user = \DB::table('users')->where('id',$id)->first(['name','profile_img','created_at']);
    $coll1 = collect(json_encode(['name' => $user->name, 'profile_img' => $user->profile_img, 'created_at' => $user->created_at]));
    $collection = $collection->concat($coll1);
    $collection = $collection->concat(\DB::table('user_info')->where('user_id',$id)->get());
    return $collection;
  })->where('id','[0-9]+');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : SKILL ROUTES
  -------------*/
  Route::get('/get-skills/{id}/{skip}','SkillController@index')
    ->where(['id'=>'[0-9]+','skip'=>'[0-9]+']);

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : QUESTIONS ROUTES
  -------------*/


  Route::get('/get_all_questions/{skip}',function($skip){
    return \App\Question::whereRaw('
      category_id = ANY (
        SELECT category_id FROM
        user_category WHERE
        user_id = '.\Auth::id().'
      )
    ')
    ->take(15)
    ->skip($skip)
    ->latest()
    ->with('user')
    ->with('category')
    ->get();

    // return \App\Question::with('user')->latest()->take(15)->skip($skip)->get();
  })->where('skip','[0-9]+');

  Route::get('/get_all_questions_for_not_auth/{skip}',function($skip){
    return \App\Question::with('user')->with('category')->latest()->take(15)->skip($skip)->get();
  })->where('skip','[0-9]+');

  Route::get('/get_all_questions_by_category/{skip}/{category_id}',function($skip,$category_id){

    return \App\Question::with('user')->latest()->where('category_id',$category_id)->take(15)->skip($skip)->get();

  })->where(['category_id'=>'[0-9]+','skip'=>'[0-9]+']);

  Route::get('/get_all_questions_by_user/{skip}/{user_id}',function($skip,$user_id){

    return \App\Question::with('user')->latest()->where('user_id',$user_id)->take(15)->skip($skip)->get();

  })->where(['user_id'=>'[0-9]+','skip'=>'[0-9]+']);

  Route::get('/get_latest_commented_posts/{user_id}',function($user_id){
    return \App\Comment::latest()->where('user_id',$user_id)->with('question')->take(10)->get();
  })->where(['user_id' => '[0-9]+']);

  Route::get('questions/{id}','QuestionController@show')->where('id','[0-9]+');

  Route::get('/get_random_questions/{skip}/{take}',function($skip,$take){
    // skip($skip)->take($take)
    return \DB::table('questions')->where('created_at','<',date("Y-m-d", strtotime( '+7 days' ) ))->orderBy('likes','desc')->take(10)->get(['id','title','comments']);
  })->where(['skip' => '[0-9]+','take' => '[0-9]+']);

  Route::get('/get_questions_in_the_same_category/{category_id}',function($category_id){
    return \DB::table('questions')->where('category_id',$category_id)->latest()->take(10)->get(['id','title','comments']);
  })->where('category_id','[0-9]+');

  Route::get('/get_common_questions',function(){
    return \DB::table('questions')
    ->where('updated_at','<',date("Y-m-d", strtotime( '+7 days' ) ))
    ->orderBy('comments','DESC')
    ->take(5)
    ->get(['id','title','comments']);
  });

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : COMMENTS PAGE ROUTES
  -------------*/

  Route::get('comments/{question_id}','CommentController@show')->where('question_id','[0-9]+');
  // replies are here too
  Route::get('/comments/{parent_comment_id}/{question_id}','ReplyController@index')
    ->where(['parent_comment_id' => '[0-9]+', 'question_id' => '[0-9]+']);

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : ALL UPDATE EXAM ROUTES
  -------------*/

  Route::get('/get_exam/{id}',function($id){
    $result = \App\Exam::whereId($id)->with('category')->get();
    return json_encode($result);
  })->where('id','[0-9]+');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : TAGS ROUTES
  -------------*/
  // Route::get('/get_old_tags',function(){
  //     $data = \DB::table('tags')->get(['tag']);
  //     return json_decode($data);
  // });

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : CATEGORIES ROUTES
  -------------*/

  Route::get('/get_old_categories',function(){

    if(!\Auth::check()) return \DB::table('categories')->get(['id','category']);

    return \App\Category::whereRaw('
      id = ANY (
        SELECT category_id FROM
        user_category WHERE
        user_id = '.\Auth::id().'
      )
    ')
    ->get();
      /*$data = \DB::table('categories')->get(['id','category']);
      return json_decode($data);*/
  });

  Route::get('/get_categories',function(){
    return \DB::table('categories')->get(['id','category']);
  });

  /*|
    |
    |
    |
    |
    |
    |
    |
    |
    -------------
    START : REMEMBER EXAMS LIST ROUTES
    -------------*/

    /*|
      |
      |
      |
      |
      |
      |
      |
      |
      -------------
      START :  SHOW EXAM ROUTES
      -------------*/

    Route::get('/getExam/{id}',function($id){
        $data = \App\Exam::whereId($id)->with('category')->get();
        return json_encode($data);
    })->where('id','[0-9]+');

    /*|
      |
      |
      |
      |
      |
      |
      |
      |
      -------------
      START :  SEARCH ROUTES
      -------------*/

      Route::post('my_search/{count}','ExamController@search')->where('count','[0-9]+');


      // Route::get('/{vue_capture?}', function () {
      //     return view('welcome');
      // })->where('vue_capture', '[\/\w\.-]*');


      Route::get('/activate-account/{token}','Auth\RegisterController@activateAccount');

      Route::get('/get-current-ads',function(){
        return \DB::table('ads')->get();
      });

      Route::get('/dashboard/{vue_capture?}', function () {
        return view('home');
      })->where('vue_capture', '(.*)')->middleware('auth','role:1');

      Route::get('/{vue_capture?}', function () {
        return view('welcome');
      })->where('vue_capture', '(.*)');
