<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maxim extends Model
{
    protected $fillable = ['topic','user_id'];

    public function user(){
      return $this->belongsTo('App\User');
    }
}
