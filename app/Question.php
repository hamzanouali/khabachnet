<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['user_id','title','category_id','topic','is_closed','comments'];

    // user that owns the question
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function tag(){
      return $this->belongsToMany('App\Tag','tag_question');
    }

    public function like(){
      return $this->hasMany('App\Like');
    }

    public function category(){
      return $this->belongsTo('App\Category');
    }

    public function comment(){
      return $this->hasMany('App\Comment');
    }
}
