<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirebaseUser extends Model
{
    protected $table = 'firebase_users';

    protected $fillable = ['id','name','email','remember_token'];
}
