<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['category'];

    public function exam(){
      return $this->hasMany('App\Exam');
    }

    public function question(){
      return $this->hasMany('App\Question');
    }
}
