<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RememberList extends Model
{
    protected $table = "remember_list";

    protected $fillable = ['title','user_id','exam_id'];

    public function remeberList(){
        return $this->belongsTo('User');
    }
}
