<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Exam extends Model
{
    // use Searchable;

    protected $table = 'exams';
    protected $fillable = [
      'title',
      'language',
      'question',
      'answer',
      'timer',
      'category_id',
      'tags',
      'background',
      'with_prediction',
      'predicted_answers',
      'predicted_messages',
    ];

    public function category(){
      return $this->belongsTo('App\Category');
    }

    public function user(){
      return $this->belongsTo('App\Exam');
    }
}
