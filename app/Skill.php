<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['user_id','name','value'];

    public function user(){
      return $this->belongsTo('App\User');
    }
}
