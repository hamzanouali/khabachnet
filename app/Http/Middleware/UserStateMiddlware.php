<?php

namespace App\Http\Middleware;

use Closure;

class UserStateMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\DB::table('users')->where('email',$request->email)->first()->is_active == 0){
            return response()->json([ 'email' => ' هذا الحساب موقف، يرجى مراسلة الإدارة للمزيد من التفاصيل ' ]);
        }

        return $next($request);
    }
}
