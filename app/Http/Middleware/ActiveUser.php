<?php

namespace App\Http\Middleware;

use Closure;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()['is_active'] == 2){
          return '401-blocked';
        }else if(\Auth::user()['is_active'] == 0){
          return '401-not-activated';
        }
        return $next($request);
    }
}
