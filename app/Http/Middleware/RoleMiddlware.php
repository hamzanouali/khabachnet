<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        // $roles :
        // 0: user
        // 1: admin
        if(\Auth::user()['role'] != $role) return redirect('/');
        return $next($request);
    }
}
