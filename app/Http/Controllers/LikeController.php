<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'user_id' => 'required|Numeric',
          'target_id' => 'required|Numeric',
          'action' => 'required|string',
          'target_type' => 'required|string'
        ]);

        // owner can't like it's posts
        if($request->user_id == \Auth::id()) return '401-owner';

        if($request->action != 'like') {

          // this will ensure our values that are comming from unexcpected users
          $request->action = 'dislike';
        }

        $arr = [
          'user_id' => \Auth::id(),
          'target_id' => $request->target_id,
          'target_type' => $request->target_type,
          'action' => $request->action
        ];

        \DB::beginTransaction();

        // last action from the user, if he is already liked/disliked that target, this variable will be filled
        $last_action = null;

        // user can't like or dislike more than once
        $data = \DB::table('likes')
          ->where('user_id',$arr['user_id'])
          ->where('target_id',$arr['target_id'])
          ->where('target_type',$arr['target_type'])
          // ->where('action',$arr['action'])
          ->first(['action']);
        // return json_encode($data);
        if($data == null) {

          \DB::table('likes')->insert($arr);
          $last_action = 'null';

        }else{
          // return json_encode($data->action);
          $last_action = $data->action;
        }

        // target_type ( question or comment )
        switch ($arr['target_type']) {

          // the target is a question
          case 'question':

            // action : like
            if($arr['action'] == 'like') {

              if($last_action == 'dislike'){
                // dislike a question
                \DB::table('questions')->whereId($arr['target_id'])->increment('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->increment('likes');
                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'null' ]);
              }else if($last_action == 'null'){
                \DB::table('questions')->whereId($arr['target_id'])->increment('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->increment('likes');
                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'like' ]);
              }

            }else if($arr['action'] == 'dislike'){


              if($last_action == 'like'){

                // dislike a question
                \DB::table('questions')->whereId($arr['target_id'])->decrement('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->decrement('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'null' ]);
              }else if($last_action == 'null'){

                // check the authorization for dislike
                $user = \DB::table('user_info')->where('user_id',\Auth::user()['id'])->first();
                if( (($user->comments*0.25) + ($user->questions*0.5) + ($user->likes)) < 250 ) {
                  return '401-dislike';
                }

                \DB::table('questions')->whereId($arr['target_id'])->decrement('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->decrement('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'dislike' ]);
              }

            }
            $data = \DB::table('questions')->where('id',$arr['target_id'])->first(['likes']);
            break;
          case 'comment':
            // action : like
            if($arr['action'] == 'like') {

              if($last_action == 'dislike'){
                // dislike a question
                \DB::table('comments')->whereId($arr['target_id'])->increment('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->increment('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'null' ]);
              }else if($last_action == 'null'){
                \DB::table('comments')->whereId($arr['target_id'])->increment('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->increment('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'like' ]);
              }

            }else if($arr['action'] == 'dislike'){

              if($last_action == 'like'){

                // dislike a question
                \DB::table('comments')->whereId($arr['target_id'])->decrement('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->decrement('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'null' ]);
              }else if($last_action == 'null'){

                // check the authorization for dislike
                $user = \DB::table('user_info')->where('user_id',\Auth::user()['id'])->first();
                if( (($user->comments*0.25) + ($user->questions*0.5) + ($user->likes)) < 250 ) {
                  return '401-dislike';
                }

                \DB::table('comments')->whereId($arr['target_id'])->decrement('likes');
                \DB::table('user_info')->where('user_id',$request->user_id)->decrement('likes');

                // update the correspondong column in (likes) table
                \DB::table('likes')
                  ->where('user_id',$arr['user_id'])
                  ->where('target_id',$arr['target_id'])
                  ->where('target_type',$arr['target_type'])
                  ->update(['action' => 'dislike' ]);
              }

            }
            $data = \DB::table('comments')->where('id',$arr['target_id'])->first(['likes']);
            break;
        }

        \DB::commit();
        return $data->likes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
