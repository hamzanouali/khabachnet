<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getTmpExamsExistance(){
      $result = \DB::table('tmp_exams')->count();
      return json_encode($result);
    }

    public function search(Request $request,$count){

        $this->validate($request,[
          // 'search_array' => 'bail|required|string'
          'string' => 'bail|required|string'
        ]);

        // $string_array = explode(' ',$request->search_string,3);
        // $string_array = json_decode($request->search_array);

        $data =  \DB::table('exams')
        ->where('title','LIKE','%'.$request->string.'%')->skip($count)->take(15)->get(['id','title','category','background']);
        // ->orWhere('tags','LIKE','%'.$string_array[1].'%')
        // ->orWhere('tags','LIKE','%'.$string_array[2].'%')->get(['id','title','background']);
        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $this->validate($request,[
            'title' => 'required|string',
            'questions' => 'required|string',
            'answers' => 'required|string',
            'timer' => 'required|string',
            'category' => 'required|Numeric',
            // 'tags' => 'required|string',
            'background' => 'required|string',
            'with_prediction' => 'required|boolean',
            'predicted_answers' => 'string',
            'predicted_messages' => 'string
            '
        ]);

        // $request->tags = str_replace('"','',$request->tags);
        // $request->tags = str_replace('[','',$request->tags);
        // $request->tags = str_replace(']','',$request->tags);
        // $request->tags = str_replace(',',' ',$request->tags);

        $exam = new Exam([
            'language' => $_COOKIE['language'],
            'title' => $request->title,
            'question' => $request->questions,
            'answer' => $request->answers,
            'timer' => $request->timer,
            'category_id' => $request->category,
            // 'tags' => $request->tags,
            'background' => $request->background,
            'with_prediction' => $request->with_prediction,
            'predicted_answers' => $request->predicted_answers,
            'predicted_messages' => $request->predicted_messages
        ]);

        $exam->save();

        if($request->id){
            \DB::table('tmp_exams')->whereId($request->id)->delete();
        }

        return 'true';

    }

    public function updateTmp(Request $request){
      $this->validate($request,[
          'title' => 'required|string',
          'questions' => 'required|string',
          'answers' => 'required|string',
          'timer' => 'required|string',
          'category' => 'required|Numeric',
          'tags' => 'required|string'
      ]);

      $exam_array = [
        'title'=> $request->title,
        'question' => $request->questions,
        'answer' => $request->answers,
        'timer' => $request->timer,
        'category_id' => $request->category,
        'tags' => $request->tags,
        'background' => ' '
      ];

      if($request->id){
          \DB::table('tmp_exams')->whereId($request->id)->update($exam_array);
          $id = $request->id;
      }else{
          $id = \DB::table('tmp_exams')->insertGetId($exam_array);
      }

      return json_encode($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request,[

          'id' => 'required|integer|min:0',
          'title' => 'required|string',
          'questions' => 'required|string',
          'answers' => 'required|string',
          'timer' => 'required|string',
          'category_id' => 'required|Numeric',
          'tags' => 'required|string',
          'with_prediction' => 'required|integer|digits_between:0,1',
          'predicted_answers' => 'required|string',
          'predicted_messages' => 'required|string',
          'background' => 'string'
      ]);

      $exam_array = [
        'id'=> $request->id,
        'title'=> $request->title,
        'question' => $request->questions,
        'answer' => $request->answers,
        'timer' => $request->timer,
        'category_id' => $request->category,
        'tags' => $request->tags,
        'background' => $request->background,
        'with_prediction' => $request->with_prediction,
        'predicted_answers' => $request->predicted_answers,
        'predicted_messages' => $request->predicted_messages
      ];

      $res = \DB::table('exams')->whereId($request->id)->update($exam_array);


      return json_encode($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request,[
          'id' => 'required|integer'
        ]);

        $result = \DB::table('exams')->whereId($request->id)->delete();

        return json_encode($result);
    }

    public function destroyTmp(Request $request)
    {
        $this->validate($request,[
          'id' => 'required|integer'
        ]);

        $result = \DB::table('tmp_exams')->whereId($request->id)->delete();

        return json_encode($result);
    }
}
