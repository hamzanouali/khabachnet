<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class SitemapController extends Controller
{
    public function index()
	{
	  $questions = \DB::table('questions')->latest()->get();

	  return response()->view('sitemap.index', [
		  'questions' => $questions
	  ])->header('Content-Type', 'text/xml');
	}
}
