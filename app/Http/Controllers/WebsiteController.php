<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function updateAds(Request $request){

      $this->validate($request,[
        'ads1' => 'string',
        'ads2' => 'string',
        'ads3' => 'string',
        'ads4' => 'string'
      ]);

      if(!\DB::table('ads')->get()->count()){

        \DB::table('ads')->insert([
          'ads1' => $request->ads1,
          'ads2' => $request->ads2,
          'ads3' => $request->ads3,
          'ads4' => $request->ads4
        ]);

      }else {
        \DB::table('ads')->update([
          'ads1' => $request->ads1,
          'ads2' => $request->ads2,
          'ads3' => $request->ads3,
          'ads4' => $request->ads4
        ]);
      }

      return 'true';

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInstructions(Request $request)
    {
        $this->validate($request,[
          'content' => 'required|json',
        ]);
        if(\DB::table('website')->get(['instructions_of_use'])->count() == 0){
          \DB::table('website')->insert(['instructions_of_use' => $request->content]);
        }
        \DB::table('website')->update(['instructions_of_use' => $request->content]);
        return 'true';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePostingRules(Request $request)
    {
        $this->validate($request,[
          'content' => 'required|json',
        ]);
        if(\DB::table('website')->get(['posting_rules'])->count() == 0){
          \DB::table('website')->insert(['posting_rules' => $request->content]);
        }
        \DB::table('website')->update(['posting_rules' => $request->content]);
        return 'true';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
