<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id,$skip)
    {
        return \DB::table('skills')->where('user_id',$id)->orderBy('value')->skip($skip)->take(3)->get();
    }

    public function updateUserInfo(Request $request){
      $this->validate($request,[
        'user_id' => 'required|Numeric',
        'name' => 'required|string',
        'description' => 'required|string',
        'country' => 'required|string',
        'links' => 'required|string',
        'maxim' => 'string'
      ]);

      // user can change it's own data only
      if(\Auth::id() != $request->user_id) return '';

      $arr = [
        'description' => $request->description,
        'country' => $request->country,
        'links' => $request->links,
        'maxim' => $request->maxim
      ];

      if($request->name != \Auth::user()['name']){
        \App\User::where('id',\Auth::id())->update(['name'=>$request->name]);
      }

      \DB::table('user_info')->where('user_id',\Auth::id())->update($arr);
      return 'true';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'name' => 'required|string',
          'value' => 'required|Numeric'
        ]);

        // prepare the name of the skill (shouldn't contain the word 'exam' or 'test')
        /*$request->name = str_replace($request->name,'إمتحان',''); // Arabic
        $request->name = str_replace($request->name,'إختبار',''); // Arabic
        $request->name = str_replace($request->name,'test','');
        $request->name = str_replace($request->name,'Test','');
        $request->name = str_replace($request->name,'Exam','');
        $request->name = str_replace($request->name,'exam','');*/

        \App\Skill::create([
          'name' => $request->name,
          'value' => $request->value,
          'user_id' => \Auth::id()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
