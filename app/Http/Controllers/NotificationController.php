<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexNotReaded($skip){
      return \DB::table('notifications')
        ->where('notifiable_id',\Auth::id())
        ->skip($skip)
        ->take(10)
        ->get(['data','created_at','id','read_at']);
    }

    public function deleteAChunk(Request $request){
      \DB::beginTransaction();
      for ($i=0; $i <count($request->object); $i++) {
        if(preg_match('/([0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+)/',$request->object[$i]['id']) == 1)
        {
          \DB::table('notifications')
            ->where('id','=',$request->object[$i]['id'])
            ->delete();
        }
        else
        {
          \DB::rollback();
          throw new \Exception("Error :(", 422);
        }
      }
      \DB::commit();
      return 'true';
    }

    public function markAsReadChunck(Request $request){
      \DB::beginTransaction();
      for ($i=0; $i <count($request->object); $i++) {
        if(preg_match('/([0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+\-[0-9a-zA-Z]+)/',$request->object[$i]['id']) == 1)
        {
          $not = \DB::table('notifications')
            ->where('id','=',$request->object[$i]['id'])
            ->update(['read_at'=>\Carbon\Carbon::now()]);
        }
        else
        {
          \DB::rollback();
          throw new \Exception("Error :(", 422);
        }
      }
      \DB::commit();
      return 'true';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
