<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Question;
use App\Comment;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index($skip)
    // {
    //     return Question::with('user')->latest()->take(2)->skip($skip)->get();
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'title' => 'required|string',
          'topic' => 'required|string',
          'topic_text' => 'required|string',
          'category_id' => 'required|Numeric'
        ]);

        $id = \Auth::id();
        if(\Auth::user()['role'] == 2){
            $id = \DB::table('users')->where('role',2)->inRandomOrder()->first()->id;
        }

        $res = new Question([
          'user_id' => $id,
          'category_id' => $request->category_id,
          'title' => $request->title,
          'topic' => $request->topic
        ]);
        $res->save();
        \DB::table('user_info')->where('user_id',\Auth::id())->increment('questions');

        return json_encode($res->id);
    }

    /**
     * close target (question, comment)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function closeTarget(Request $request)
    {
        $this->validate($request,[
          'target_id' => 'required|Numeric',
          'target_type' => 'required|string',
          'user_id' => 'required|Numeric'
        ]);

        if($request->user_id != \Auth::id()) return 'false';

        switch ($request->target_type) {
          case 'comment':
            \DB::table('comments')->where('id',$request->target_id)->update(['is_closed'=>1]);
            break;

          case 'question':
            \DB::table('questions')->where('id',$request->target_id)->update(['is_closed'=>1]);
            break;
        }

        return 'true';
    }

    /**
     * open target (question, comment)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function openTarget(Request $request)
    {
        $this->validate($request,[
          'target_id' => 'required|Numeric',
          'target_type' => 'required|string',
          'user_id' => 'required|Numeric'
        ]);

        if($request->user_id != \Auth::id()) return 'false';

        switch ($request->target_type) {
          case 'comment':
            \DB::table('comments')->where('id',$request->target_id)->update(['is_closed'=>0]);
            break;

          case 'question':
            \DB::table('questions')->where('id',$request->target_id)->update(['is_closed'=>0]);
            break;
        }

        return 'true';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return Question::whereId($id)->with('user')->with('category')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'user_id' => 'required|Numeric',
          'title' => 'required|string',
          'topic' => 'required|json',
          'topic_text' => 'required|string',
          'category_id' => 'required|Numeric'
        ]);

        if(\Auth::id() != $request->user_id && \Auth::user()['role'] == 0) return false;

        \DB::table('questions')->where('id',$id)->update([
          'title'=> $request->title,
          'topic'=> $request->topic,
          'category_id' => $request->category_id
        ]);

        return 'true';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
