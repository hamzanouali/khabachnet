<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     public function login(Request $request){
       $this->validate($request,[
         'email' => 'required|email',
         'password' => 'required|string'
       ]);

       if(!\DB::table('users')->where('email',$request->email)->get()->count()){
         $errors = new \Illuminate\Support\MessageBag();

         // add your error messages:
         $errors->add('email', __('auth.failed'));

         return view('auth.login')->withErrors($errors);
       }
       // if(\DB::table('users')->where('email',$request->email)->first()->is_active == 2){
       //   return redirect('/activate-account');
       // }
       $binary = 0;
       if(\DB::table('users')->where('email',$request->email)->first()->role != 1) $binary = 1;
       // login the activated users only and remember them all the time
       if (\Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active'=>1],$binary)) {
             // Authentication passed...
             return redirect('/');
       }else{
         if(\DB::table('users')->where('email',$request->email)->get()->count() == 0){
            $errors = new \Illuminate\Support\MessageBag();

            // add your error messages:
            $errors->add('email', __('auth.failed'));

            return view('auth.login')->withErrors($errors);
         }else if(\DB::table('users')->where('email',$request->email)->first()->is_active == 0){
            $errors = new \Illuminate\Support\MessageBag();

            // add your error messages:
            $errors->add('email', __('auth.blocked'));

            return view('auth.login')->withErrors($errors);
         }else{
            $errors = new \Illuminate\Support\MessageBag();

            // add your error messages:
            $errors->add('email', __('auth.failed'));

            return view('auth.login')->withErrors($errors);
         }
       }
     }

}
