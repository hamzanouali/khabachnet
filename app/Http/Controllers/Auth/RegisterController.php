<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Crypt;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/ar/categories/follow';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            // for gravatar
            'profile_img' => md5(trim($data['email'])),
            'password' => bcrypt($data['password'])
        ]);
        \DB::table('user_info')->insert([
          'user_id' => $user->id,
          'description' => '...',
          'country' => '...',
          'maxim' => '...',
          'links' => '[]',
          'comments' => 0,
          'questions' => 0,
          'likes' => 0

        ]);
        // return $user->email;
        // Mail::to($user->email)->send(new \App\Mail\AccountActivation(Crypt::encryptString($user->email)));

        return $user;
    }

    public function activateAccount($token){
      if(\DB::table('users')->where('email',Crypt::decryptString($token))->get()->count() > 0){
        \DB::table('users')->where('email',Crypt::decryptString($token))->update(['is_active'=>1]);
      }else{
        // redirect to random route
        return redirect('/qsmdlkmqsld');
        // then will be redirected to 404 page by laravel automaticely
      }
      return redirect('/login')->with('status', ' تم تفعيل حسابك بدون مشاكل، يمكنك الدخول الآن ');
    }

}
