<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'replies';

    protected $fillable = ['user_id','question_id','parent_comment_id','replies','title','topic','likes'];

    // user that owns the reply
    public function user(){
      return $this->belongsTo('App\User');
    }
}
