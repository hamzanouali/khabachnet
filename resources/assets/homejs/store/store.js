require("babel-polyfill")
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// importing dictionary
import dictionary from './language';

export const store = new Vuex.Store({

  // edit or call state element in strict mode (getters and mutations)
  strict : true,

  state: {

    // is this user logged-in ?

    fromState_isAuth : '',

    user: {
      name : null,
      id : null
    },
    // must login ?
    pageRequireAuth: '',

    gapi: '',

    // last browsed route (used to go back after login or sign up)
    last_route: '/',

    // Reminded Me is:  remember_me
    // remember me will be array after filling it like:
    // remember_me_list: ['title1','title2']
    remember_me_list : 'empty',

    notifications: [],

    // open/close side modal
    sideModal: false,

    open_remember_modal: false,
    open_notifications_modal: false,

    categories : [],
    categories_in_dropdown : [],

    // used to aviod repeatation in Reminded Me list (remember_me)
    arr_titles_remember_me_list : [],

    // wich language we are using now in the template
    language: 'ar',

    state_dictionary: dictionary,

    // dictionary contain all the languages and its constent texts and word in our template
    lang : dictionary.ar,

    // light/night mode
    night_mode : 1,
  },

  // getters
  // called by: sotre.getters.name_of_element
  getters :{
    fromState_isAuth : state => {
      return state.fromState_isAuth;
    },
    user : state => {
      return state.user;
    },
    pageRequireAuth: state => {
      return state.pageRequireAuth;
    },
    gapi: state => {
      return state.gapi;
    },
    last_route: state => {
      return state.last_route;
    },
    remember_me_list: state => {
      return state.remember_me_list;
    },
    notifications: state => {
      return state.notifications;
    },
    arr_titles_remember_me_list: state => {
      return state.arr_titles_remember_me_list;
    },
    sideModal: state => {
      return state.sideModal;
    },
    open_remember_modal: state => {
      return state.open_remember_modal;
    },
    open_notifications_modal: state => {
      return state.open_notifications_modal;
    },
    categories: state => {
      return state.categories;
    },
    categories_in_dropdown: state => {
      return state.categories_in_dropdown;
    },
    language: state => {
      return state.language;
    },
    lang: state => {
      return state.lang;
    },
    state_dictionary: state => {
      return state.state_dictionary;
    },
    night_mode: state => {
      return state.night_mode;
    }
  },

  // mutations
  // called by: store.commit('name_of_mutation',values..)
  mutations: {
    fromState_isAuth: (state,bool_val) =>{
      state.fromState_isAuth = bool_val;
    },
    user: (state,obj) =>{
      state.user = obj;
    },
    pageRequireAuth : (state,bool_val) => {
      state.pageRequireAuth = bool_val;
    },
    gapi : (state,object) => {
      state.gapi = object;
    },
    last_route: (state,string) => {
      state.last_route = string;
    },
    remember_me_list: (state,object) => {
      state.remember_me_list.push(object);
    },
    notifications: (state,object) => {
      state.notifications = state.notifications.concat(object);
    },
    arr_titles_remember_me_list : (state,string) => {
      state.arr_titles_remember_me_list.push(string);
    },
    remove_remember_me_list: state => {
      state.remember_me_list = [];
    },
    remove_notifications: state => {
      state.notifications = [];
    },
    mark_as_read: state => {
      for(let i = 0; i< state.notifications.length; i++){
        state.notifications[i].read_at = new Date();
      }
    },
    sideModal: (state,bool_val) => {
      state.sideModal = bool_val;
    },
    open_remember_modal: (state,bool_val) => {
      state.open_remember_modal = bool_val;
    },
    open_notifications_modal: (state,bool_val) => {
      state.open_notifications_modal = bool_val;
    },
    categories: (state,arr) => {
      state.categories = arr;
    },
    categories_in_dropdown: (state,arr) => {
      state.categories_in_dropdown = arr;
    }
    ,
    language: (state,language) => {
      state.language = language;

      switch (language) {
        case 'en':
          state.lang = dictionary.en;
          break;

        case 'ar':
          state.lang = dictionary.ar;
          break;

        default: state.lang = dictionary.en;
      }

    },
    night_mode: (state,bool_val) => {
      if(state.night_mode == 1) {
        let script = document.createElement('link');
        script.setAttribute('rel','stylesheet');
        script.setAttribute('href','css/night_mode.css');
        document.getElementsByTagName('head')[0].appendChild(script);
        state.night_mode = 0;
      }else{
        document.getElementsByTagName('head')[0].lastChild.href = '#';
        state.night_mode = 1;
      }
    }
  }

});
