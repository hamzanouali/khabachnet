import VueRouter from 'vue-router';
import axios from 'axios';
import { store } from './store/store';
import apiHelpers from './api_helpers';

// get all cookies and convert it to json
let getCookiesAsJson = ()=>{

  let cookie_str = document.cookie;
  cookie_str = cookie_str.replace(/;/g,'","');
  cookie_str = cookie_str.replace(/=/g,'":"');
  cookie_str = cookie_str.replace(/\s/g,'');
  cookie_str = '{"'+cookie_str+'"}';
  return JSON.parse(cookie_str);

}

if(getCookiesAsJson().hasOwnProperty('language')) {
  document.cookie = "language="+store.getters.language+"; expires= Infinity";
  store.commit('language',store.getters.language);
}else{
  document.cookie = "language="+store.getters.language+"; expires= Infinity";
}

console.log(store.getters.language);

let routes = [

  {
    path: '/logout',
    beforeEnter : (to,from,next)=> {
      return window.location.href = "/logout";
    }
  },

  {
    path : '/dashboard/',
    redirect: '/dashboard/'+store.getters.language
  },

  {
    path : '/dashboard/:language',
    name: 'dashboard',
    beforeEnter: (to,form,next)=>{
      // check wether the passed param to :language does not exists in dictionary
      if(store.getters.state_dictionary.hasOwnProperty(to.params.language)) return next();
      else return next(store.getters.language + '/404-page-not-found');
    },
    component : require('./user/home'),
  },

  {
    path: '/dashboard/exam/post',
    component : require('./user/post'),
    name : 'postExam'
  },

  {
    path : '/dashboard/create-tags',
    name : 'createTags',
    component : require('./user/create')
  },

  {
    path : '/dashboard/create-categories',
    name : 'createCategories',
    component : require('./user/create_cat')
  },

  {
    path : '/dashboard/exams',
    name : 'exams',
    component : require('./user/update_exams')
  },

  {
    path : '/dashboard/update_tmp_exam/:id?',
    name : 'update_tmp_exam',
    component : require('./user/post')
  },

  {
    path : '/dashboard/update_exam/:id?/:title?',
    name : 'update_exam',
    component : require('./user/post')
  },

  {
    path: '/dashboard/users',
    name: 'users',
    component: require('./user/users')
  },

  {
    path: '/dashboard/questions',
    name: 'dashQuestions',
    component: require('./user/questions')
  },

  {
    path : '/dashboard/ads',
    name : 'ads',
    component : require('./user/ads')
  },

  {
    path : '/dashboard/instructions-of-use',
    name : 'instructions',
    component : require('./instructions')
  },

  {
    path : '/dashboard/posting-rules',
    name : 'pRules',
    component : require('./postingRules')
  },

];

/////////////////////////////////////////////////
// to avoid '/:language' repeatation in each path
/////////////////////////////////////////////////
for (var i = 3; i < routes.length; i++) {
  routes[i].path = routes[i].path.replace('/dashboard/','/dashboard/:language/');
}
/////////////////////////////////////////////////

export default new VueRouter({
  // this code for jumping in the same page using #to-tag
  scrollBehavior: function(to, from, savedPosition) {
      if (to.hash) {
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
  mode: 'history',
  routes
});
