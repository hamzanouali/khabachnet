import Vue from 'vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
// import 'buefy/lib/buefy.css'
import 'font-awesome/css/fontawesome-all.min.css'
import 'nprogress/nprogress.css'
import NProgress from 'nprogress/nprogress.js'
import apiHelpers from './api_helpers.js'
import Meta from 'vue-meta'
var firebase = require('firebase');
var firebaseui = require('firebaseui');

import 'highlight.js/styles/tomorrow.css'
var hljs = require('highlight.js')
window.hljs = hljs;

hljs.configure({   // optionally configure hljs
  // recommended for quill
  useBR: false
});

require('quill/dist/quill.snow.css');
var Quill = require('quill/dist/quill.min.js');
window.Quill = Quill;
Vue.use(Meta);
// import moment.js and set the locale language to Arabic (Algeria)
var moment = require('moment');
moment.locale('ar-dz');
// moment.locale('en-us');
//
window.moment = moment;

Vue.use(Buefy)

window.NProgress = NProgress;

window.Vue = Vue;
window.apiHelpers = apiHelpers;
window.firebase = firebase;

// window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

// try {
//     window.$ = window.jQuery = require('jquery');
//
//     require('bootstrap-sass');
// } catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

Vue.use(VueRouter)
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
