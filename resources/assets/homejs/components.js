import dashboard from './dashboard.vue';
// import questions from './views/questions/questions.vue';

Vue.component('dashboard',dashboard);
// Vue.component('questions',questions);

Vue.component('my-header',{

  template: `
    <div>
      <section class="hero is-primary is-medium is-bold">
        <div class="hero-body">
          <div class="container">
            <div class="is-hidden-desktop" style="height:100px"></div>
            <h1 dir="rtl" class="title">
                {{ $store.getters.lang.title_head }}
            </h1>
            <h2 dir="rtl" class="subtitle p-t-20">
                {{ $store.getters.lang.text_head }}
            </h2>
          </div>
        </div>
      </section>
      <div class="searchBigContainer">
        <section class="container buttnav">
          <div>
            <form action="" class="columns">
              <div class="field searchButtField">
                <div class="control">
                  <button class="button is-large is-primary"> <i class="fas fa-search"></i> </button>
                </div>
              </div>
              <div class="column field is-full">
                <div class="control">

                <input type="text" ref="search" v-model="string" v-bind:placeholder="language.search_input_placeholder" class="input is-large homeSearch radius-0">

                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  `,

  data(){
    return {
      language : this.$store.getters.lang,
      string: null
    }
  },

  watch : {
    string : function(value){
      window.location.href = '/'+this.$store.getters.language+'/search='+value;
    }
  },

});

Vue.component('timer',{

  props: ['minutes','seconds','runIt','timeReady'],

  template : `
    <div v-if="timeReady" :class="clock_class">
      <span>
        <i class="fas fa-clock m-t-9"></i>  {{ min | twoDigit }}
      </span>
      <span >:</span>
      <span>{{ sec | twoDigit }}</span>
    </div>
  `,
  data(){
    return {
      clock_class : 'general_clock green_clock',
      min : this.minutes,
      sec : this.seconds,
      interval : '',
    }
  },

  filters: {
    twoDigit : value => {
        if(value % 10 == value){
            return '0'+value;
        }else{
            return value;
        }
    }
  },

  watch : {
    runIt : function(value){
      if(value && this.timeReady){
        this.start();
      }else if(value == false){
        clearInterval(this.interval);
      }
    }
  },

  methods: {

    start(){

        let interval = setInterval( ()=>{


            if(this.sec <= 30 && this.sec > 10 && this.min == 0){
              this.clock_class = "general_clock yellow_clock";
            }

            if(this.sec <= 10 && this.min == 0){
              this.clock_class = "general_clock";
            }

            if(this.sec != 0){
                this.sec--;
            }else{
                if(this.min != 0){
                    this.min--;
                    this.sec = 59;
                }else{
                  this.$emit('time-out',true);
                    clearInterval(interval);
                }
            }
        },1000);
        this.interval = interval;

    }

  }
});

Vue.component('notification-component',{
  template : `
  <div v-if="$store.getters.open_notifications_modal == true && $store.getters.user.id" class="columns sideHiddenDiv notificationsModal second-one third-one is-mobile">
    <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile contDiv">

      <!-- deleting button -->
      <span ref="removeNot" @click="deleteAllNotifications()" id="removeRememberList" class="button is-default">
        <span> حذف الكل </span>
        <i class="fas fa-trash"></i>
      </span>

      <!-- -->
      <div v-for="notification in $store.getters.notifications" class="eachTitle notificationDiv">

          <div v-if="notification.read_at != null" class="columns is-multiline is-mobile">
            <span class="column is-12">
              <a @click="toQuestionShowPage(JSON.parse(notification.data))">
                 {{ JSON.parse(notification.data).content }}
              </a>
            </span>
            <span class="column is-12 p-t-0 p-b-0"> {{ JSON.parse(notification.data).user }} </span>
            <span class="column is-12 p-t-0"> {{ notification.created_at }} </span>
          </div>
          <div v-if="notification.read_at == null" style="background:#eee;" class="columns is-multiline is-mobile">
            <span class="column is-12">
              <a @click="toQuestionShowPage(JSON.parse(notification.data))">
                 {{ JSON.parse(notification.data).content }}
              </a>
            </span>
            <span class="column is-12 p-t-0 p-b-0"> {{ JSON.parse(notification.data).user }} </span>
            <span class="column is-12 p-t-0"> {{ notification.created_at }} </span>
          </div>
      </div>

      <div class="text-center p-5 m-t-10">
        <span ref="moreNot" class="button is-small" @click="getMoreNotifications()"> المزيد </span>
      </div>

    </div>

    <div class="column" @click="$store.commit('open_notifications_modal',false);$store.commit('sideModal',false)" style="background:none">
    </div>
  </div>
  `,

  data(){
    return {
    }
  },

  mounted(){
    if(this.$store.getters.notifications.length) this.markNotificationsAsRead();
  },

  methods: {
    toQuestionShowPage(object){
      return window.location.href = '/questions/'+object.question_id+'/'+object.content.replace(/[\/\\\s]/g,'-');
    },

    getMoreNotifications(){
      this.$refs.moreNot.className = 'button is-small is-loading';
      axios.get('/not-readed-notifications/'+this.$store.getters.notifications.length)
        .then(res => {
          if(res.data.length) this.$store.commit('notifications',res.data);
          else this.$refs.moreNot.className = 'button is-small is-static';
        });
    },

    deleteAllNotifications(){
      this.$refs.removeNot.className = 'button is-default is-loading';
      axios.post('/deleteAllNotifications',{ object: this.$store.getters.notifications })
        .then(res => {

            this.$refs.removeNot.className = 'button is-default';
            this.$store.commit('remove_notifications');

        })
        .catch(err => {
          this.$refs.removeNot.className = 'button is-default';
        })
    },

    //markAsReadSomeNotifications
    markNotificationsAsRead(){
      axios.post('/markAsReadSomeNotifications',{ object: this.$store.getters.notifications })
        .then(res => {

          this.$store.commit('mark_as_read');

        })
        .catch(err => {
          // //
        })
    }
  }

});

Vue.component('remember-list-component',{
  template: `
  <div v-if="$store.getters.open_remember_modal && $store.getters.fromState_isAuth" class="columns sideHiddenDiv second-one third-one is-mobile">
    <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile contDiv">

      <!-- deleting button -->
      <span v-if="!remove_remember_me_list_in_progress" @click="deleteRememberList" id="removeRememberList" class="button is-default">
        <span> حذف الكل </span>
        <i class="fas fa-trash"></i>
      </span>

      <!-- deleting button in progress -->
      <span v-if="remove_remember_me_list_in_progress" id="removeRememberList" class="button is-loading is-default">
      </span>

      <!-- -->
      <div v-for="elem in $store.getters.remember_me_list" class="eachTitle">
        <a :href="'/questions/'+elem.id+'/'+title: elem.title.replace(/[\/\\\s]/g,'-')">
            {{ elem.title }}
        </a>
      </div>

    </div>

    <div class="column" @click="$store.commit('open_remember_modal',false)" style="background:none">
    </div>
  </div>
  `,
  data(){
    return {
      remove_remember_me_list_in_progress: false,
    }
  },
  methods: {
    deleteRememberList(){
      this.remove_remember_me_list_in_progress = true;
      axios.get('/deleteRememberList')
        .then(res => {

            this.remove_remember_me_list_in_progress = false;
            this.$store.commit('remove_remember_me_list');
            this.counted_value = 0;

        })
        .catch(err => {
          //
        })
    },
  }
})

// fixed navbar and fixed side bar
Vue.component('nav-component',{
    template : `
    <div>
      <div ref="navbar" class="columns my-navbar-container">

        <div class="column my-left-side-nav">
          <ul>
            <li>
              <a @click="openSideModal" class="left-first navbar-item"><i class="fas fa-list"></i> </a>
            </li>
            <li v-if="$store.getters.user.id">
              <a href="/questions/post" class="left-first navbar-item">
                <i class="fas fa-plus"></i>
              </a>
            </li>
            <li>
              <div class="dropdown is-hoverable translation-button-div">
                <!--
                <div class="dropdown-trigger">
                  <button class="button is-info" aria-haspopup="true" aria-controls="dropdown-menu4">
                    <span>Arabic</span>
                    <span class="icon is-small">
                      <i class="fas fa-language" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div class="dropdown-menu" id="dropdown-menu4" role="menu">
                  <div class="dropdown-content">
                    <a @click="setLanguage('ar')" class="dropdown-item">
                      العربية
                    </a>
                    <a @click="setLanguage('en')" class="dropdown-item">
                      English
                    </a>
                    <a @click="setLanguage('fr')" class="dropdown-item">
                      Français
                    </a>
                  </div>
                </div>
                -->
              </div>
            </li>
            <li v-if="$store.getters.user.id" class="is-relative p-t-5 m-t-15" @click="$store.commit('sideModal',true);$store.commit('open_notifications_modal',true);">
              <div v-if="$store.getters.notifications">
                <div v-if="notificationsExist()" class="notification-mark"></div>
              </div>
              <a class="m-l-15"><i class="fas fa-bell"></i></a>
            </li>
            <!--<li class="night-mode-button">
              <a @click="$store.commit('night_mode','01')">🌓</a>
            </li>-->
          </ul>
        </div>

        <div class="column is-10 my-right-side-nav">
          <ul>
            <li>
              <a href="/">
                <a class="first navbar-item"><i class="fas fa-home"></i></a>
              </a>
            </li>
            <li class="is-hidden-mobile">
              <div class="chessQueen navbar-item has-dropdown is-hoverable">
                <div class="navbar-link" style="border-left: 1px solid #444846;">
                  <i class="fas fa-chess-queen"></i>
                </div>
                <div id="moreDropdown" class="navbar-dropdown" style="left: -167px;margin-top:1px;">
                  <a class="navbar-item" to="/#questions">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/question.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم المناقشات و الأسئلة </strong>
                          <br>
                          <small> قسم يسمح لك بمناقشة أفكارك و طريح أسئلة لمشاكلك. </small>
                        </p>
                      </div>
                    </div>
                  </a>
                  <hr class="navbar-divider">
                  <a class="navbar-item " to="#">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/flask.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم الإستفتاءات </strong>
                          <br>
                          <small> تحت التطوير... </small>
                        </p>
                      </div>
                    </div>
                  </a>

                </div>
              </div>
            </li>
            <li v-for="cat in $store.getters.categories" class="is-hidden-mobile">
                  <a :href="'/by-category/'+cat.id+'/'+cat.category.replace(/[\/\\\s]/g,'-')">
                      <a class="first navbar-item">{{ cat.category }}</a>
                  </a>
            </li>

            <li class="is-relative is-hidden-mobile">
              <a @click="modal = true" v-if="$store.getters.categories_in_dropdown.length>0" class="first navbar-item">
              المزيد
              <i class="fas fa-angle-left m-b-5 p-r-5 is-primary"></i>
              </a>
              <div v-if="dropdown" class="dropdown-more">
                <ul>
                  <li v-for="cat in $store.getters.categories_in_dropdown">
                    <a class="first navbar-item">
                      <a :href="'by-category'+cat.id+'/'+cat.category.replace(/[\/\\\s]/g,'-')">
                          {{ cat.category }}
                      </a>
                    </a>
                  </li>
                </ul>
              </div>
            </li>

            <li @click="modal = true" class="is-hidden-tablet is-hidden-desktop">
              <a class="first navbar-item">{{ $store.getters.lang.categories }}</a>
            </li>

          </ul>
        </div>

      </div>

      <div v-if="modal" class="modal is-active">
        <div class="modal-background" @click="modal = false"></div>
        <div class="modal-content">

        <aside class="menu">
          <p class="menu-label">
            {{ $store.getters.lang.categories }}
          </p>
          <ul class="menu-list">
            <li v-for="cat in $store.getters.categories_in_dropdown.concat($store.getters.categories)">
              <a :href="'/by-category/'+cat.id+'/'+cat.category.replace(/[\/\\\s]/g,'-')">
                  {{ cat.category }}
              </a>
            </li>
            <li>
              <div class="chessQueen navbar-item has-dropdown is-hoverable">
                <div id="moreDropdown" class="navbar-dropdown" style="left: -167px;margin-top:1px;">
                  <a class="navbar-item ">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/question.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم المناقشات و الأسئلة </strong>
                          <br>
                          <small> قسم يسمح لك بمناقشة أفكارك و طريح أسئلة لمشاكلك. </small>
                        </p>
                      </div>
                    </div>
                  </a>
                  <hr class="navbar-divider">
                  <a class="navbar-item ">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/flask.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم الإختبارات </strong>
                          <br>
                          <small> يمنحك هذا القسم الفرصة لإمتحان معارفك. </small>
                        </p>
                      </div>
                    </div>
                  </a>

                </div>
              </div>
            </li>
          </ul>
        </aside>


        </div>
        <button @click="modal = false" class="modal-close is-large" aria-label="close"></button>
      </div>


      <!--

      #/_ DESKTOP NAVBAR ************************

      ---->
      <nav class="navbar is-transparent is-hidden">

          <div id="navMenuTransparentExample" class="navbar-menu">

            <div class="navbar-start">
              <a class="navbar-item" @click="openSideModal">
                <i class="fas fa-list"></i>
              </a>
            </div>

            <div class="navbar-end txt-right">

              <div class="navbar-item has-dropdown is-hoverable">
                <a v-if="$store.getters.categories_in_dropdown.length > 0" class="navbar-item ">
                  المزيد
                  <i class="fas fa-angle-left is-primary"></i>
                </a>
                <div class="navbar-dropdown is-boxed">

                  <span class="navbar-item" v-for="cat in $store.getters.categories_in_dropdown">
                  <a :href="'/by-category/'+cat.id+'/'+cat.category.replace(/[\/\\\s]/g,'-')">
                      {{ cat.category }}
                  </a>
                  </span>

                </div>
              </div>
              <li v-for="cat in $store.getters.categories">
                    <a :href="'/by-category/'+cat.id+'/'+cat.category.replace(/[\/\\\s]/g,'-')">
                        <a class="first navbar-item">{{ cat.category }}</a>
                    </a>
              </li>

            </div>

          </div>
        </nav>


      <!--

      #/_ SIDE MODAL ****************************

      ---->

      <div v-if="$store.getters.sideModal">
        <!-- logged in modal -->
        <!-- remind me/ dashboard/ ..etc icons -->
        <div class="columns sideHiddenDiv is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile divOverFlow">

            <!--<p @click="$store.commit('open_remember_modal',true)" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon">
                <div class="addOnSpan">
                  <span :class="counted_class" v-text="$store.getters.remember_me_list.length"></span>
                </div>
                <i class="fas fa-clock"></i>
              </span>
              <span class="column is-12 the-text"> الإجتياز لاحقا </span>
            </p>-->

            <a href="/dashboard"><p v-if="$store.getters.user.role == 1" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-desktop"></i></span>
              <span class="column is-12 the-text"> لوحة التحكم </span>
            </p></a>

            <p @click="this.window.location.href = '/users/'+$store.getters.user.id+'/'+$store.getters.user.name.replace(/[\/\\\s]/g,'-')" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-cog"></i></span>
              <span class="column is-12 the-text"> إعداداتي </span>
            </p>

            <p @click="this.window.location.href = '/instructions-of-use'" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-info-circle"></i></span>
              <span class="column is-12 the-text"> إرشادات </span>
            </p>

            <p @click="this.window.location.href = '/logout'" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-sign-out-alt"></i></span>
              <span class="column is-12 the-text"> خروج </span>
            </p>

          </div>

          <div class="column" @click="closeModal" style="background:none">
          </div>
        </div>

        <!-- login/ register modal -->
        <div v-if="$store.getters.fromState_isAuth == false" class="columns sideHiddenDiv second-one is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile">

            <!-- sign-in button -->
            <span class="button is-default">
              <a href="/login">
              <span> تسجيل الدخول </span>
              <!--<span class="fas fa-envelope"></span>-->
              </a>
            </span>
            <!-- create account -->
            <span class="button is-default">
              <a href="/register">
              <span> إنشاء حساب </span>
              <!--<span class="fas fa-handshake"></span>-->
              </a>
            </span>

            <!-- <p> أو يمكنك إستخدام حسابك في </p>

            Google sign-in button
            <span class="button is-default">
              <span> Google </span>
              <span class="fab fa-google"></span>
            </span>-->

            <!--Facebook sign-in button
            <span class="button is-default is-info" style="color:#fff !important;">
              <span> Facebook </span>
              <span class="fab fa-facebook"></span>
            </span>-->

          </div>

          <div class="column" @click="$store.commit('sideModal',false)" style="background:none">
          </div>
        </div>

        <remember-list-component></remember-list-component>

        <notification-component></notification-component>

      </div>
    </div>
    `,
    data() {
      return {
        sideModal : false,
        open_remember_modal : false,
        counted_value : 0,
        counted_class : 'button is-primary counted-btn',
        categories : [],
        categories_in_dropdown : [],
        dropdown : false,
        modal : false,
      }
    },
    updated(){
      this.notificationsExist();
    },
    mounted(){
      if(this.$store.getters.categories.length == 0) this.getOldCategories();

    },

    methods: {
      notificationsExist(){
        for (let i = 0; i < this.$store.getters.notifications.length; i++) {
          if(this.$store.getters.notifications[i].read_at == null){
            return true;
          }
        }
        return false;
      },

      setLanguage(language){
          document.cookie = "language="+language+"; expires= Infinity";
          this.$store.commit('language',language);
          window.location.replace('/');
      },

      catModal(){
        if(this.modal){
          this.modal = false;
        }else{
          this.modal = true;
        }
      },

      openDropDwon(){
        if(!this.dropdown){
          this.dropdown = true;
        }else{
          this.dropdown = false;
        }
      },

      closeModal(){
        this.$store.commit('open_remember_modal',false);
        this.$store.commit('sideModal',false);
      },

      openSideModal(){
        this.$store.commit('sideModal',true);
        if(this.$store.getters.remember_me_list == 'empty' && this.$store.getters.fromState_isAuth == true){
          this.counted_class = 'button is-primary counted-btn is-loading';

          // get remember list from database
          axios.get('/getRememberList')
            .then(res => {
              this.counted_class = 'button is-primary counted-btn';
              this.$store.commit('remove_remember_me_list');
              this.$store.commit('remove_remember_me_list');
              res.data.forEach(element => {
                this.$store.commit('remember_me_list',{ title : element.title, id: element.id } );
              });
              this.counted_value = this.$store.getters.remember_me_list.length;
            })
            .catch(err => {
              //
            });
        }
      },

      getOldCategories(){
        axios.get('/get_old_categories')
          .then(res => {
              if(res.status == 200){

                if(res.data.length > 6){
                  let categories_in_dropdown = res.data;
                  this.$store.commit('categories',categories_in_dropdown.slice(0,7));
                  this.$store.commit('categories_in_dropdown',res.data.slice(7,res.data.length));
                }else{
                  this.$store.commit('categories',res.data);
                }

              }
          })
          .catch(err => {

          });
      }

    }
  });


// exam card
// show the exam in squared card
Vue.component('exam-card',{
  // exam props contain object of data
  // example :
  // { id:.. ,title:'...', background: '...' ...etc}

  props:['exam'],
  template : `
  <div
    :style="JSON.parse(exam.background).card_back"
    class="card" style="box-shadow: 0 8px 19px #d4d4d4;">

    <div id="cardcontent" class="card-content" :style="JSON.parse(exam.background).background">

      <span class="category-span">
        <a :href="'/by-category/'+exam.cat.id+'/'+exam.category.category.replace(/[\/\\\s]/g,'-')">
            {{ exam.category.category }}
        </a>
      </span>
      <h1 ref="titlecustomizing" class="title" dir="rtl" align="center">
         {{ exam.title }}
      </h1>

    </div>
    <footer class="card-footer">
      <p class="card-footer-item item-1">
        <span>
          <a @click="toShowPage(exam.id,exam.title)"> {{ $store.getters.lang.test_now }} </a>
        </span>
      </p>
      <p class="card-footer-item">
        <span>
          <a @click="addToRememberMe({ title : exam.title, id: exam.id })"> {{ $store.getters.lang.remember_me }} </a>
        </span>
      </p>
    </footer>
  </div>
  `,
  data(){
    return {
      edit_exam: false,
      open_exam_dropdown: false,
    }
  },
  methods:{
    toShowPage(id,title){
      title = title.replace(/[\/\\\s]/g,'-');
      window.location.href = '/show/'+id+'/'+title;
    },

    openSideModal(){
        this.$store.commit('sideModal',true);
        if(this.$store.getters.remember_me_list == 'empty'){
          this.counted_class = 'button is-primary counted-btn is-loading';
          axios.get('/getRememberList')
            .then(res => {
              this.counted_class = 'button is-primary counted-btn';
              this.$store.commit('remove_remember_me_list');
              this.$store.commit('remove_remember_me_list');
              res.data.forEach(element => {
                this.$store.commit('remember_me_list',element.title);
              });
              this.counted_value = this.$store.getters.remember_me_list.length;
            })
            .catch(err => {
              //
            });
      }
    },

    addToRememberMe(object){
        this.openSideModal();
          // checking user authentication
          if(this.$store.getters.fromState_isAuth){
            // open modal
            this.$store.commit('open_remember_modal',true);
            // wait until we get the remember list from database
            let interval = setInterval(()=>{
              // checking if we got some data from database
              if(this.$store.getters.remember_me_list != 'empty'){
                // checking if the title exists in the remember list
                if(this.$store.getters.arr_titles_remember_me_list.indexOf(object.title) == -1){
                  // insert a title to database
                  axios.post('/addToRememberList',{
                    title : object.title,
                    exam_id : object.id
                  }).then(res => {
                      if(res.data == true){
                        this.$store.commit('remember_me_list',object);
                        this.$store.commit('arr_titles_remember_me_list',object.title);
                      }else{
                      }
                    })
                    .catch();
                    clearInterval(interval);
                }
              }
            },50);
            // end waiting
          }
    },
  }
});

// modified beufy checkbox
Vue.component('mb-checkbox',{
  props: ['nativeValue','checked'],
  template : `
    <div id="root">
      <div>
      <b-checkbox ref="checkbox" :checked="my_checked"
          :native-value="my_nativeValue" v-model="value">
          <div @click="switchCheckbox()" style="width:100%;">
            <div class="new-check-icon" style="
                position:  absolute;
                font-size: 23px;
                left: 11px;
                top: -4px;
                right:0px;
            ">
              <i ref="square" class="far fa-square"></i>
            </div>
            <slot></slot>
          </div>
      </b-checkbox>
      </div>
    </div>
  `,
  data() {
    return {
      value: null,
      my_checked: this.checked,
      my_nativeValue: this.nativeValue,
    }
  },
  methods: {
    switchCheckbox(){
       if(this.$refs.square.className != 'far fa-square') this.$refs.square.className = 'far fa-square';
       else this.$refs.square.className = 'fas fa-check-square';
       this.$emit('clicked_val',this.value);
    }
  }
});

// statistics's circle
Vue.component('circle-stat',{
  props: ['skill','progress_obj'],
  template : `
  <div id="root">
    <div class="circle-statistics" ref="hola"></div>
      <p dir="rtl" style="
          padding: 5px;
          font-weight:  bold;
          color: #929292;
      ">{{ m_skill.name }}</p>
    </div>
  </div>
  `,
  data(){
    return {
      // my skill : m_skill
      m_skill : this.skill,
      // progress object will be imported in the .vue page
      progress: this.progress_obj
    }
  },
  mounted(){
    // progressbar.js@1.0.0 version is used
    // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

    var cir = new this.progress.Circle(this.$refs.hola, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1000,
      color: '#2b2b2b',
      trailColor: '#eee',
      trailWidth: 1,
      text: this.m_skill.text + '%',
      svgStyle: null,
      text: {
        value: this.m_skill.value*100 + '%',
      }
    });

    // cir.animate(1.0);  // Number from 0.0 to 1.0
    // number of all right answers / the right checked answes
    // ex: 20/ 10 = 2.0

    cir.animate(this.m_skill.value, {
          duration: 1000,
      }, function() {

    });
  }
});


// my amazing button
Vue.component('am-btn',{
  template: `
  <div style="width:100px;" class="am-btn-root">
    <button @click="$emit('click',true)" class="button is-default is-centered m-t-15">
      <slot></slot>
    </button>
    <div class="back-line"></div>
  </div>
  `
});



Vue.component('update-quill-content',{
  props: ['topic','question_title','category','comment_id'],
  template: `
  <div class="columns m-t-15 is-multiline is-mobile">
    <a class="column is-12 smallUnderlinedText" @click="open_modal='display:block'"> تعديل </a>

    <div :style="open_modal" class="column is-12 modal">
      <div class="modal-background" @click="open_modal='display:none'"></div>
      <div class="modal-card m-t-30" style="margin:auto">

        <section class="modal-card-body">
          <!-- Content ... -->

          <div class="columns is-multiline">
            <div v-if="this.question_title" class="column is-12 post-label">
              <label> العنوان: </label>
            </div>
            <div v-if="this.question_title" class="column is-12 m-t-0 p-t-0">
              <input placeholder="مثلا: لدي مشكلة في تثبيت البرنامج..." type="text" v-model="title">
            </div>

            <div v-if="this.category" class="column is-12">
              <!-- category -->
              <b-field
                  label="التصنيف">
                  <b-select v-model="selected_category" class="select-cat" placeholder="تصنيف" expanded>
                      <option v-for="cat in this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories)" :value="cat.id">{{ cat.category }}</option>
                  </b-select>
              </b-field>
            </div>
            <div class="column is-12 post-label m-t-25">
              <label> النص: </label>
            </div>
            <div class="column is-12 p-t-0 m-t-0">
              <div ref="QuillArea"></div>
            </div>
          </div>


          <!-- Content ... -->
        </section>
        <footer style="background:#1b1b1b;" dir="ltr" class="modal-card-foot">
          <button ref="submitBTN" class="button is-primary" @click="update()"> تحديث </button>
          <button class="button is-light" @click="open_modal='display:none'"> إلغاء </button>
        </footer>
      </div>
    </div>
  </div>
  `,

  data(){
    return {
      open_modal: 'display:none',
      title: this.question_title,
      selected_category: this.category,
      quill:null,
    }
  },

  mounted(){
    // hljs.initHighlightingOnLoad();
    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax: true,
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold'], //'italic', 'underline'
          ['code-block','blockquote'],
          ['link']
        ],
      },
      theme: 'snow',
      formats : [
        'background',
        'bold',
        'color',
        'font',
        'code',
        'italic',
        'link',
        'size',
        'strike',
        'script',
        'underline',
        'blockquote',
        'header',
        'indent',
        'list',
        'align',
        'direction',
        'code-block',
        'formula'
        // 'image'
        // 'video'
      ]
    });
    this.quill.setContents(JSON.parse(this.topic));
  },
  watch: {
    open_modal(val){
      if(val == 'display:none'){
        this.quill.setContents(JSON.parse(this.topic));
      }
    }
  },
  methods: {
    update(){
      this.$refs.submitBTN.className='button is-primary is-loading';
      // question case
      if(this.question_title && this.category){
        axios.post('/update_a_question/'+this.$route.params.id,{
          user_id: this.$store.getters.user.id,
          title : this.title,
          category_id : this.selected_category,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              title : this.title,
              category : this.selected_category,
              topic: JSON.stringify(this.quill.getContents())
            });
          }
        })
        .catch(err => {
          this.$refs.submitBTN.className='button is-primary';
          this.$snackbar.open({
              message: ' تأكد من ملئ جميع البيانات ',
              type: 'is-danger',
              position: 'is-bottom',
              actionText: 'إغلاق',
              indefinite: true,
              onAction: () => {

              }
          });
        });

      // comment case
      }else{
        axios.post('/update_a_comment/'+this.comment_id,{
          user_id: this.$store.getters.user.id,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              topic: JSON.stringify(this.quill.getContents())
            });
          }
        })
        .catch(err => {
        });
      }

    }
  }

});

Vue.component('Quill-bubble-show',{
  props: ['topic'],
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null
    }
  },

  watch: {
    topic:function(val){
      if(this.quill.getText().length){
        this.quill.setContents(JSON.parse(val));
      }
    }
  },

  mounted(){
    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'bubble',
      readOnly: true,
      modules: {
        syntax: true
      },
    });
    this.quill.setContents(JSON.parse(this.topic));
    // call computed property (ttopic) for setting contents to quill
    // this.ttopic;

  }
});

Vue.component('Quill-Snow-Editor',{
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null,
    }
  },

  mounted(){
    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'snow',
      readOnly: true,
      modules: {
        syntax: true,              // Include syntax module
        toolbar: [['code-block']]  // Include button in toolbar
      },
      formats : [
        'background',
        'bold',
        'color',
        'font',
        'code',
        'italic',
        'link',
        'size',
        'strike',
        'script',
        'underline',
        'blockquote',
        'header',
        'indent',
        'list',
        'align',
        'direction',
        'code-block',
        'formula'
        // 'image'
        // 'video'
      ]
    });

    let interval = setInterval(()=>{
      if(this.topic) {
        this.quill.setContents(JSON.parse(this.topic));
        clearInterval(interval);
      }
    },10);
  }

});

// category selector component

Vue.component('category-selector',{
  props: ['label','size','placeholder','v-model'],
  template: `
  <div class="field">
    <div class="control">
      <b-field
          :label="this.label">
          <b-select @input="emitValue" v-model="selected_category" :size="this.size" class="select-cat" :placeholder="this.placeholder" expanded>
              <option v-for="cat in this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories)" :value="cat.id">{{ cat.category }}</option>
          </b-select>
      </b-field>
    </div>
  </div>
  `,

  data(){
    return {
      // categories selector
      categories: this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories),
      selected_category: null,
      // watch_selected_category : this.selected_category,
    }
  },

  methods : {
    emitValue(){
      this.$emit('input',this.selected_category);
    }
  }

});

Vue.component('hamza-toast',{
  props: ['text','LightButton','PrimaryButton','done'],
  template: `
  <div :style="style" class="hamza-toast root loginDiv columns is-multiline is-mobile">
    <div class="column is-12">
      <p>{{ this.text }}</p>
    </div>
    <hr>
    <div class="column is-12" style="text-align:center;">
      <span class="button is-light m-r-5" @click="cancel()"> {{ this.LightButton }} </span>
      <span ref="submit" class="button is-primary" @click="toggle();$emit('click',true)"> {{ this.PrimaryButton }} </span>
    </div>
  </div>
  `,
  data(){
    return {
      style : 'bottom:-500px'
    }
  },

  methods: {
    cancel(){
        this.$emit('cancel',true);
        this.$refs.submit.className = 'button is-primary';
        this.style = 'bottom:-500px;transition:0.5s';
    },
    toggle(){
      this.$refs.submit.className += ' is-loading';
    }
  },

  watch: {
    done(value){
      if(value) {
        this.$refs.submit.className = 'button is-primary';
      }
    }
  }
});
