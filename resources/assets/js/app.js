require('./bootstrap.js');
import router from './routes.js';
import { store } from './store/store';
require('./components.js');
import routesHandler from './routes_handler';
import VueAnalytics from 'vue-analytics'


/*
------------------------------------------------------------------------------------------
here i am catching all the 'matas' from the current route that i am trying to enter it now
and then do what should i do.
for example: if the route require authentication, i will check if i am logged in
or i will redirect myself to the login page.
------------------------------------------------------------------------------------------
*/

router.beforeEach( (to,from,next)=>{
  NProgress.start();
  if(
    to.path.split('/')[1] != store.getters.language &&
    to.path.split('/')[1] != 'login' &&
    to.path.split('/')[1] != 'register' &&
    to.path.split('/')[1] != 'password'
  ) return next("/"+store.getters.language+to.path);
  if(to.matched.length == 0) {
    return next("/"+store.getters.language+"/404-page-not-found");
  }

  // save the last route
  if(to.name != 'fullReplies'){
    store.commit('last_route',to.path);
  }

  let obj = new routesHandler(to,from,next);
  obj.checkForAuthentication();
  if(window.location.href.split('/')[3] == 'login#' || window.location.href.split('/')[3] == 'register#' || window.location.href.split('/')[3] == 'password') {
    NProgress.done();
  }

  // get the not readed notification
  if(store.getters.user.id != undefined) {
    axios.get('/not-readed-notifications/'+store.getters.notifications.length)
    .then(res => {
      if(res.data.length != 0) store.commit('notifications',res.data);
    });
  }

} );

router.afterEach((to, from) => {
  if(to.params.title){
    document.title = to.params.title.replace(/-/g,' ');
  }else {
    document.title = "خباش | Khabach.net";
  }
});

Vue.use(VueAnalytics, {
  id: 'UA-117769569-1',
  router
});

new Vue({
  beforeCreate(){
    if(store.getters.fromState_isAuth == ''){
      axios.get('/isAuth')
      .then(res => {

        if(res.data.id) {
          store.commit('fromState_isAuth',true);
          store.commit('user',res.data);

          if(res.data.is_active && res.data.is_active == '2'){
            this.$snackbar.open({
                duration : 999999999,
                message: ' حسابك محظور، ليس بإمكانك النشر أو التفاعل بالموقع بعد الآن ',
                type: 'is-danger',
                position: 'is-bottom',
                actionText: 'إغلاق',
                indefinite: true,
                onAction: () => {
                    // this.$toast.close();
                }
            });
          }else if(res.data.is_active && res.data.is_active == '0'){
            this.$snackbar.open({
                duration : 999999999,
                message: ' أرسلنا رابط تفعيل حسابك إلى بريدك الإلكتروني ',
                type: 'is-warning',
                position: 'is-bottom',
                actionText: 'إغلاق',
                indefinite: true,
                onAction: () => {
                    // this.$toast.close();
                }
            });
          }

          // get the not readed notification
          if(store.getters.user.id != undefined) {
            axios.get('/not-readed-notifications/0')
            .then(res => {
              if(res.data.length != 0) store.commit('notifications',res.data);
            });
          }

        }else{
          store.commit('fromState_isAuth',false);
        }
      });
    }
  },
  el : '#app',
  router,
  store,
});

/**
 * Uncomment below when compiling to production
 */
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;
