

export default {
    getFacebookSDK(){
        window.fbAsyncInit = function() {
            FB.init({
              appId            : '137391850214593',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : 'v2.10'
            });
            FB.AppEvents.logPageView();
          };

        (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "http://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    },

    getGoogleGapi(){
        let script = document.createElement('script');
        script.setAttribute('src','https://apis.google.com/js/platform.js');
        script.setAttribute('async','');
        script.setAttribute('defer','');
        document.getElementsByTagName('body')[0].appendChild(script);
    },

    getFirebase(elementReferring){

        // Initialize Firebase

        if(firebase.apps.length == 0){
            var config = {

                apiKey: "AIzaSyCJykgYAcvEy24mYnmHyZFQDtC9Ar-UkKA",
                authDomain: "firstapp-180610.firebaseapp.com",
                databaseURL: "https://firstapp-180610.firebaseio.com",
                projectId: "firstapp-180610",
                storageBucket: "firstapp-180610.appspot.com",
                messagingSenderId: "697146948664"

            };

            firebase.initializeApp(config);
        }

        if(elementReferring)
        {
            //firebase.initializeApp(config);

         // FirebaseUI config.
              var uiConfig = {
                signInSuccessUrl: '#',
                signInOptions: [
                  // Leave the lines as is for the providers you want to offer your users.
                  firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                  firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                  //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
                  //firebase.auth.GithubAuthProvider.PROVIDER_ID,
                  //firebase.auth.EmailAuthProvider.PROVIDER_ID,
                  //firebase.auth.PhoneAuthProvider.PROVIDER_ID
                ],
                // Terms of service url.
                tosUrl: '#'
              };

              // Initialize the FirebaseUI Widget using Firebase.
              var ui = new firebaseui.auth.AuthUI(firebase.auth());
              // The start method will wait until the DOM is loaded.

              ui.start(elementReferring.firebaseui, uiConfig);
        }

    }

}
