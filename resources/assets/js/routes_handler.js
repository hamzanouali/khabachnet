require('./bootstrap.js');
import router from './routes.js';
import { store } from './store/store';

export default class routesHandler {

  // to_object is specified by the beforeEach router's method
  constructor(to_object,from_object,next_object){
    this.to = to_object;
    this.from = from_object;
    this.next = next_object;
  }

  // language handler
  // return string (path)
  // checkAndSetLanguage(){
  //   if(this.to.path.split('/')[1] != store.getters.language) return "/"+store.getters.language+this.to.path;
  //   if(this.to.matched.length == 0) {
  //     return "/"+store.getters.language+"/404-page-not-found";
  //   }
  // }

  // check for the authorization by role (ex: admin)
  // return boolean
  checkForAuthorizationByRole(){
    if(this.to.meta.admin == true){
      if(store.getters.user.role === undefined) {
        // NProgress.start();
        let counter = 500;
        let interval = setInterval( () => {
          if(counter == 10000 || store.getters.user.role != undefined && store.getters.user.role != 1) {
            clearInterval(interval);
            // not authorized
            return this.next('/404-page-not-found');
          }else if(store.getters.user.role == 1) {
            clearInterval(interval);
            // authorized
            return this.next();
          }
          counter += 500;
        },500);
      }
      else if(store.getters.user.role == 1){
        return this.next();
      }else {
        // not authorized
        return this.next('/404-page-not-found');
        // return this.next('/404-page-not-found');
      }
    }
  }

  // check for authentication
  // return boolean
  checkForAuthentication(){
    NProgress.set(0.3);
    if(this.to.meta.isAuth == true){
      store.commit('pageRequireAuth',true);
      this.checkForAuthorizationByRole();

      let counter = 500;
      let interval = setInterval( () => {
        if(store.getters.user.id > 0) {
          clearInterval(interval);
          // authorized
          return this.next();
        }
        
        if(!store.getters.user.id && counter == 5000) window.location.href = '/login';
        counter += 500;
      },500);

      // if(!this.to.meta.admin){
      //   if(store.getters.fromState_isAuth == ''){
      //     axios.get('/isAuth')
      //     .then(response => {
      //       if(response.data == '') window.location.href = "/login";
      //       store.commit('fromState_isAuth',JSON.parse(response.data));
      //       if(store.getters.fromState_isAuth == false){
      //         return this.next('/404-page-not-found');
      //       }else if(store.getters.fromState_isAuth == true){
      //         return this.next();
      //       }else{
      //         console.log('oops...');
      //       }
      //     });
      //   }else if(store.getters.fromState_isAuth == false){
      //     return this.next('/404-page-not-found');
      //   }else if(store.getters.fromState_isAuth == true){
      //     return this.next();
      //   }
      // }

    }else{
      return this.next();
      // if(this.to.name == 'login' || this.to.name == 'register'){
      //   if(store.getters.fromState_isAuth == true){
      //     return next('/');
      //   }else{
      //     let imported = document.createElement('script');
      //     imported.src = 'https://apis.google.com/js/platform.js';
      //     document.head.appendChild(imported);
      //     return next();
      //   }
      // }else{
      //   return next();
      // }
    }

  }

}


// this code may be pasted inside the beforeEach


// if(to.path.split('/')[1] != store.getters.language) return "/"+store.getters.language+to.path;
// if(to.matched.length == 0) {
//   return "/"+store.getters.language+"/404-page-not-found";
// }
// if(
//   // to.path != '/'+store.getters.language+'/login' &&
//   // to.path != '/'+store.getters.language+'/register' &&
//   // to.path != '/'+store.getters.language+'/logout' &&
//   // to.path != '/login' &&
//   // to.path != '/register' &&
//   // to.path != '/logout'&&
//   to.name != 'fullReplies'
//   ){
//   store.commit('last_route',to.path);
// }
//
// if(to.meta.admin == true){
//   if(store.getters.user.role === undefined) {
//     console.log('redirecting... to dashboard');
//     NProgress.start();
//     let counter = 500;
//     let interval = setInterval( () => {
//       if(counter == 10000 || store.getters.user.role != undefined && store.getters.user.role != 1) {
//         clearInterval(interval);
//         return next('/404-page-not-found');
//       }else if(store.getters.user.role == 1) {
//         clearInterval(interval);
//         return next();
//       }
//       counter += 500;
//     },500);
//   }
//   else if(store.getters.user.role != 1) return next('/404-page-not-found');
// }
//
// // if the path require authentication
// if(to.meta.isAuth == true){
//   store.commit('pageRequireAuth',true);
//   if(store.getters.fromState_isAuth == ''){
//     axios.get('/isAuth')
//     .then(response => {
//       if(response.data == '') window.location.href = "/login";
//       console.log(' please wait, we are redirecting you now.. ');
//       store.commit('fromState_isAuth',JSON.parse(response.data));
//       if(store.getters.fromState_isAuth == false){
//         return next('/404-page-not-found');
//         //return window.location.href="/login";
//       }else if(store.getters.fromState_isAuth == true){
//         return next();
//       }else{
//         console.log('---->**_o_**<-----');
//       }
//     });
//   }else if(store.getters.fromState_isAuth == false){
//     console.log('not auth user !');
//     return next('/404-page-not-found');//return next('/login');
//   }else if(store.getters.fromState_isAuth == true){
//     return next();
//   }
// }else{
//   if(to.name == 'login' || to.name == 'register'){
//     if(store.getters.fromState_isAuth == true){
//       return next('/');
//     }else{
//       let imported = document.createElement('script');
//       imported.src = 'https://apis.google.com/js/platform.js';
//       document.head.appendChild(imported);
//       return next();
//     }
//   }else{
//     return next();
//   }
// }
