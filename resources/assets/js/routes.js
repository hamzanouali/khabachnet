import VueRouter from 'vue-router';
import axios from 'axios';
import { store } from './store/store';
import apiHelpers from './api_helpers';

// get all cookies and convert it to json
let getCookiesAsJson = ()=>{

  let cookie_str = document.cookie;
  cookie_str = cookie_str.replace(/;/g,'","');
  cookie_str = cookie_str.replace(/=/g,'":"');
  cookie_str = cookie_str.replace(/\s/g,'');
  cookie_str = '{"'+cookie_str+'"}';
  return JSON.parse(cookie_str);

}

if(getCookiesAsJson().hasOwnProperty('language')) {
  document.cookie = "language="+store.getters.language+"; expires= Infinity";
  store.commit('language',store.getters.language);
}else{
  document.cookie = "language="+store.getters.language+"; expires= Infinity";
}

let routes = [

  {
    path : '/login',
    beforeEnter: (to,fom,next) => {
      NProgress.done();
    }
  },

  {
    path : '/dashboard',
    beforeEnter: (to,fom,next) => {
      NProgress.done();
    }
  },

  {
    path : '/register',
    beforeEnter: (to,fom,next) => {
      NProgress.done();
    }
  },

  {
    path : '/password/reset/:token?',
    beforeEnter: (to,fom,next) => {
      NProgress.done();
    }
  },

  {
    path : '/',
    redirect : '/'+store.getters.language,
  },

  {
    path : '/:language',
    beforeEnter: (to,form,next)=>{
      // check wether the passed param to :language does not exists in dictionary
      if(store.getters.state_dictionary.hasOwnProperty(to.params.language)) return next();
      else return next(store.getters.language + '/404-page-not-found');
    },
    component : require('./views/home')
  },

  {
    path : '/404-page-not-found',
    component : require('./views/404_page.vue')
  },

  {
    path : '/by-category/:id/:category',
    name : 'category',
    component : require('./views/by_category')
  },

  {
    path : '/about',
    component : require('./views/about')
  },

  {
    path : '/all-exams',
    name : 'allExams',
    component : require('./views/all_exams')
  },

  {
    path : '/whoops',
    component : {
      template : `
        <div style="font-size:25px;font-weight:200;text-align:center;background:none;padding:50px;">
          <p style="padding-bottom:25px;border-bottom:1px dashed #ccc">ما الذي تحاول فعله !</p>
          <router-link style="font-size:20px;" to="/"> الحساب الشخصي </router-link> -
          <router-link style="font-size:20px;" to="/"> الصفحة الرئيسية </router-link>
        </div>

      `
    }
  },

  {
    path: '/show/:id/:title',
    name : 'show',
    component : require('./views/show')
  },

  // {
  //   path: '/post/show/:id/:title',
  //   component : require('./views/user/show_for_result_cases')
  // },

  {
    path: '/logout',
    beforeEnter : (to,from,next)=> {
      return window.location.href = "/logout";
    //   axios.get('/logout')
    //     .then(res => {
    //       if(res.data == false){
    //         store.commit('fromState_isAuth',res.data);
    //         apiHelpers.getFirebase();
    //         this.$store.commit('user',{ id:null, name: null });
    //         firebase.auth().signOut().then( res => {
    //           return next(store.getters.last_route);
    //         });
    //       }
    //     })
    //     .catch(err => {
    //       console.log(err);
    //     });
    }
  },

  {
    path : '/search=:string?',
    component : require('./views/search')
  },

  {
    path: '/users/:id?/:name?',
    name: 'user',
    component: require('./views/user/profile')
  },

  {
    path: '/questions',
    name: 'questions',
    component: require('./views/questions/questions')
  },

  {
    path : '/questions/:id/:title',
    name : 'showQuestion',
    component: require('./views/questions/show')
  },

  {
    path : '/questions/:id/:title/full-replies/:commentid',
    name : 'fullReplies',
    component: require('./views/questions/full_replies')
  },

  {
    path : '/questions/post',
    name : 'post_a_question',
    component : require('./views/questions/post'),
    meta : {
      isAuth: true,
    }
  },

  {
    path : '/categories/follow',
    name : 'followCategories',
    component : require('./views/user/follow_categories'),
    meta : {
       isAuth: true,
    }
  },

  {
    path : '/instructions-of-use',
    name : 'inst',
    component : require('./views/showInstructionsOfUse')
  },

  {
    path : '/activate-account',
    name: 'activateAccount',
    component : require('./views/auth/activateaccount')
  }

];

/////////////////////////////////////////////////
// to avoid '/:language' repeatation in each path
/////////////////////////////////////////////////
for (var i = 6; i < routes.length; i++) {
  routes[i].path = "/:language"+routes[i].path;
}
/////////////////////////////////////////////////

export default new VueRouter({
  // this code for jumping in the same page using #to-tag
  scrollBehavior: function(to, from, savedPosition) {
      if (to.hash) {
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
  mode: 'history',
  routes
});
