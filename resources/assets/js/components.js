
import questions from './views/questions/questions.vue';

Vue.component('questions',questions);

Vue.component('my-header',{

  template: `
    <div>
      <section class="hero is-primary is-medium is-bold">
        <div class="hero-body">
          <div class="container">
            <div class="is-hidden-desktop" style="height:100px"></div>
            <h1 dir="rtl" class="title">
                {{ $store.getters.lang.title_head }}
            </h1>
            <h2 dir="rtl" class="subtitle p-t-20">
                {{ $store.getters.lang.text_head }}
            </h2>
          </div>
        </div>
      </section>
      <div class="searchBigContainer">
        <section class="container buttnav">
          <div>
            <form action="" class="columns">
              <div class="field searchButtField">
                <div class="control">
                  <button class="button is-large is-primary"> <i class="fas fa-search"></i> </button>
                </div>
              </div>
              <div class="column field is-full">
                <div class="control">

                <input type="text" ref="search" v-model="string" v-bind:placeholder="language.search_input_placeholder" class="input is-large homeSearch radius-0">

                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  `,

  data(){
    return {
      language : this.$store.getters.lang,
      string: null
    }
  },

  watch : {
    string : function(value){
      this.$router.push('/'+this.$store.getters.language+'/search='+value);
    }
  },

});

Vue.component('timer',{

  props: ['minutes','seconds','runIt','timeReady'],

  template : `
    <div v-if="timeReady" :class="clock_class">
      <span>
        <i class="fas fa-clock m-t-9"></i>  {{ min | twoDigit }}
      </span>
      <span >:</span>
      <span>{{ sec | twoDigit }}</span>
    </div>
  `,
  data(){
    return {
      clock_class : 'general_clock green_clock',
      min : this.minutes,
      sec : this.seconds,
      interval : '',
    }
  },

  filters: {
    twoDigit : value => {
        if(value % 10 == value){
            return '0'+value;
        }else{
            return value;
        }
    }
  },

  watch : {
    runIt : function(value){
      if(value && this.timeReady){
        this.start();
      }else if(value == false){
        clearInterval(this.interval);
      }
    }
  },

  methods: {

    start(){

        let interval = setInterval( ()=>{


            if(this.sec <= 30 && this.sec > 10 && this.min == 0){
              this.clock_class = "general_clock yellow_clock";
            }

            if(this.sec <= 10 && this.min == 0){
              this.clock_class = "general_clock";
            }

            if(this.sec != 0){
                this.sec--;
            }else{
                if(this.min != 0){
                    this.min--;
                    this.sec = 59;
                }else{
                  this.$emit('time-out',true);
                    clearInterval(interval);
                }
            }
        },1000);
        this.interval = interval;

    }

  }
});

Vue.component('notification-component',{
  template : `
  <div v-if="$store.getters.open_notifications_modal == true && $store.getters.user.id" class="columns sideHiddenDiv notificationsModal second-one third-one is-mobile">
    <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile contDiv">

      <!-- deleting button -->
      <span ref="removeNot" @click="deleteAllNotifications()" id="removeRememberList" class="button is-default">
        <span> حذف الكل </span>
        <i class="fas fa-trash"></i>
      </span>

      <!-- -->
      <div v-for="notification in $store.getters.notifications" class="eachTitle notificationDiv">

          <div v-if="notification.read_at != null" class="columns is-multiline is-mobile">
            <span class="column is-12">
              <a @click="toQuestionShowPage(JSON.parse(notification.data))">
                 {{ JSON.parse(notification.data).content }}
              </a>
            </span>
            <span class="column is-12 p-t-0 p-b-0"> {{ JSON.parse(notification.data).user }} </span>
            <span class="column is-12 p-t-0"> {{ notification.created_at }} </span>
          </div>
          <div v-if="notification.read_at == null" style="background:#eee;" class="columns is-multiline is-mobile">
            <span class="column is-12">
              <a @click="toQuestionShowPage(JSON.parse(notification.data))">
                 {{ JSON.parse(notification.data).content }}
              </a>
            </span>
            <span class="column is-12 p-t-0 p-b-0"> {{ JSON.parse(notification.data).user }} </span>
            <span class="column is-12 p-t-0"> {{ notification.created_at }} </span>
          </div>
      </div>

      <div class="text-center p-5 m-t-10">
        <span ref="moreNot" class="button is-small" @click="getMoreNotifications()"> المزيد </span>
      </div>

    </div>

    <div class="column" @click="$store.commit('open_notifications_modal',false);$store.commit('sideModal',false)" style="background:none">
    </div>
  </div>
  `,

  data(){
    return {
    }
  },

  mounted(){
    if(this.$store.getters.notifications.length) this.markNotificationsAsRead();
  },

  methods: {
    toQuestionShowPage(object){
      return this.$router.push('/questions/'+object.question_id+'/'+object.content.replace(/[\/\\\s]/g,'-'));
    },

    getMoreNotifications(){
      this.$refs.moreNot.className = 'button is-small is-loading';
      axios.get('/not-readed-notifications/'+this.$store.getters.notifications.length)
        .then(res => {
          if(res.data.length) this.$store.commit('notifications',res.data);
          else this.$refs.moreNot.className = 'button is-small is-static';
        });
    },

    deleteAllNotifications(){
      this.$refs.removeNot.className = 'button is-default is-loading';
      axios.post('/deleteAllNotifications',{ object: this.$store.getters.notifications })
        .then(res => {

            this.$refs.removeNot.className = 'button is-default';
            this.$store.commit('remove_notifications');

        })
        .catch(err => {
          this.$refs.removeNot.className = 'button is-default';
        })
    },

    //markAsReadSomeNotifications
    markNotificationsAsRead(){
      axios.post('/markAsReadSomeNotifications',{ object: this.$store.getters.notifications })
        .then(res => {

          this.$store.commit('mark_as_read');

        })
        .catch(err => {
          // //
        })
    }
  }

});

Vue.component('remember-list-component',{
  template: `
  <div v-if="$store.getters.open_remember_modal && $store.getters.fromState_isAuth" class="columns sideHiddenDiv second-one third-one is-mobile">
    <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile contDiv">

      <!-- deleting button -->
      <span v-if="!remove_remember_me_list_in_progress" @click="deleteRememberList" id="removeRememberList" class="button is-default">
        <span> حذف الكل </span>
        <i class="fas fa-trash"></i>
      </span>

      <!-- deleting button in progress -->
      <span v-if="remove_remember_me_list_in_progress" id="removeRememberList" class="button is-loading is-default">
      </span>

      <!-- -->
      <div v-for="elem in $store.getters.remember_me_list" class="eachTitle">
        <router-link :to="{ name : 'show', params: { id: elem.id , title: elem.title.replace(/[\/\\\s]/g,'-') } }">
            {{ elem.title }}
        </router-link>
      </div>

    </div>

    <div class="column" @click="$store.commit('open_remember_modal',false)" style="background:none">
    </div>
  </div>
  `,
  data(){
    return {
      remove_remember_me_list_in_progress: false,
    }
  },
  methods: {
    deleteRememberList(){
      this.remove_remember_me_list_in_progress = true;
      axios.get('/deleteRememberList')
        .then(res => {

            this.remove_remember_me_list_in_progress = false;
            this.$store.commit('remove_remember_me_list');
            this.counted_value = 0;

        })
        .catch(err => {
          //
        })
    },
  }
})

// fixed navbar and fixed side bar
Vue.component('nav-component',{
    template : `
    <div>
      <div ref="navbar" class="columns my-navbar-container">

        <div class="column my-left-side-nav">
          <ul>
            <li>
              <a @click="openSideModal" class="left-first navbar-item"><i class="fas fa-list"></i> </a>
            </li>
            <li v-if="$store.getters.user.id">
              <a @click="$router.push('/questions/post')" class="left-first navbar-item">
                <i class="fas fa-plus"></i>
              </a>
            </li>
            <li>
              <div class="dropdown is-hoverable translation-button-div">
                <!--
                <div class="dropdown-trigger">
                  <button class="button is-info" aria-haspopup="true" aria-controls="dropdown-menu4">
                    <span>Arabic</span>
                    <span class="icon is-small">
                      <i class="fas fa-language" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div class="dropdown-menu" id="dropdown-menu4" role="menu">
                  <div class="dropdown-content">
                    <a @click="setLanguage('ar')" class="dropdown-item">
                      العربية
                    </a>
                    <a @click="setLanguage('en')" class="dropdown-item">
                      English
                    </a>
                    <a @click="setLanguage('fr')" class="dropdown-item">
                      Français
                    </a>
                  </div>
                </div>
                -->
              </div>
            </li>
            <li v-if="$store.getters.user.id" class="is-relative p-t-5 m-t-15" @click="$store.commit('sideModal',true);$store.commit('open_notifications_modal',true);">
              <div v-if="$store.getters.notifications">
                <div v-if="notificationsExist()" class="notification-mark"></div>
              </div>
              <a class="m-l-15"><i class="fas fa-bell"></i></a>
            </li>
            <!--<li class="night-mode-button">
              <a @click="$store.commit('night_mode','01')">🌓</a>
            </li>-->
          </ul>
        </div>

        <div class="column is-10 my-right-side-nav">
          <ul>
            <li>
              <router-link to="/">
                <a class="first navbar-item"><i class="fas fa-home"></i></a>
              </router-link>
            </li>
            <li class="is-hidden-mobile">
              <div class="chessQueen navbar-item has-dropdown is-hoverable">
                <div class="navbar-link" style="border-left: 1px solid #444846;">
                  <i class="fas fa-chess-queen"></i>
                </div>
                <div id="moreDropdown" class="navbar-dropdown" style="left: -167px;margin-top:1px;">
                  <router-link class="navbar-item" to="/#questions">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/question.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم المناقشات و الأسئلة </strong>
                          <br>
                          <small> قسم يسمح لك بمناقشة أفكارك و طريح أسئلة لمشاكلك. </small>
                        </p>
                      </div>
                    </div>
                  </router-link>
                  <hr class="navbar-divider">
                  <router-link class="navbar-item " to="#">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/flask.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم الإستفتاءات </strong>
                          <br>
                          <small> تحت التطوير... </small>
                        </p>
                      </div>
                    </div>
                  </router-link>

                </div>
              </div>
            </li>
            <li v-for="cat in $store.getters.categories" class="is-hidden-mobile">
                  <router-link :to="{ name: 'category', params: { id: cat.id , category:cat.category.replace(/[\/\\\s]/g,'-')} }">
                      <a class="first navbar-item">{{ cat.category }}</a>
                  </router-link>
            </li>

            <li class="is-relative is-hidden-mobile">
              <a @click="modal = true" v-if="$store.getters.categories_in_dropdown.length>0" class="first navbar-item">
              المزيد
              <i class="fas fa-angle-left m-b-5 p-r-5 is-primary"></i>
              </a>
              <div v-if="dropdown" class="dropdown-more">
                <ul>
                  <li v-for="cat in $store.getters.categories_in_dropdown">
                    <a class="first navbar-item">
                      <router-link :to="{ name: 'category', params: {id:cat.id, category:cat.category.replace(/[\/\\\s]/g,'-')} }">
                          {{ cat.category }}
                      </router-link>
                    </a>
                  </li>
                </ul>
              </div>
            </li>

            <li @click="modal = true" class="is-hidden-tablet is-hidden-desktop">
              <a class="first navbar-item">{{ $store.getters.lang.categories }}</a>
            </li>

          </ul>
        </div>

      </div>

      <div v-if="modal" class="modal is-active">
        <div class="modal-background" @click="modal = false"></div>
        <div class="modal-content">

        <aside class="menu">
          <p class="menu-label">
            {{ $store.getters.lang.categories }}
          </p>
          <ul class="menu-list">
            <li v-for="cat in $store.getters.categories_in_dropdown.concat($store.getters.categories)">
              <router-link :to="{ name: 'category', params: { id: cat.id, category:cat.category.replace(/[\/\\\s]/g,'-')} }">
                  {{ cat.category }}
              </router-link>
            </li>
            <li>
              <div class="chessQueen navbar-item has-dropdown is-hoverable">
                <div id="moreDropdown" class="navbar-dropdown" style="left: -167px;margin-top:1px;">
                  <a class="navbar-item ">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/question.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم المناقشات و الأسئلة </strong>
                          <br>
                          <small> قسم يسمح لك بمناقشة أفكارك و طريح أسئلة لمشاكلك. </small>
                        </p>
                      </div>
                    </div>
                  </a>
                  <hr class="navbar-divider">
                  <a class="navbar-item ">
                    <div class="media">
                      <div class="media-left">
                        <span class="icon is-large has-text-success">
                          <img src="/images/flask.png">
                        </span>
                      </div>
                      <div class="media-content">
                        <p>
                          <strong> قسم الإختبارات </strong>
                          <br>
                          <small> يمنحك هذا القسم الفرصة لإمتحان معارفك. </small>
                        </p>
                      </div>
                    </div>
                  </a>

                </div>
              </div>
            </li>
          </ul>
        </aside>


        </div>
        <button @click="modal = false" class="modal-close is-large" aria-label="close"></button>
      </div>


      <!--

      #/_ DESKTOP NAVBAR ************************

      ---->
      <nav class="navbar is-transparent is-hidden">

          <div id="navMenuTransparentExample" class="navbar-menu">

            <div class="navbar-start">
              <a class="navbar-item" @click="openSideModal">
                <i class="fas fa-list"></i>
              </a>
            </div>

            <div class="navbar-end txt-right">

              <div class="navbar-item has-dropdown is-hoverable">
                <a v-if="$store.getters.categories_in_dropdown.length > 0" class="navbar-item ">
                  المزيد
                  <i class="fas fa-angle-left is-primary"></i>
                </a>
                <div class="navbar-dropdown is-boxed">

                  <span class="navbar-item" v-for="cat in $store.getters.categories_in_dropdown">
                  <router-link :to="{ name: 'category', params: {id: cat.id, category:cat.category.replace(/[\/\\\s]/g,'-')} }">
                      {{ cat.category }}
                  </router-link>
                  </span>

                </div>
              </div>
              <li v-for="cat in $store.getters.categories">
                    <router-link :to="{ name: 'category', params: {id: cat.id,category:cat.category.replace(/[\/\\\s]/g,'-')} }">
                        <a class="first navbar-item">{{ cat.category }}</a>
                    </router-link>
              </li>

            </div>

          </div>
        </nav>


      <!--

      #/_ SIDE MODAL ****************************

      ---->

      <div v-if="$store.getters.sideModal">
        <!-- logged in modal -->
        <!-- remind me/ dashboard/ ..etc icons -->
        <div class="columns sideHiddenDiv is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile divOverFlow">

            <!--<p @click="$store.commit('open_remember_modal',true)" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon">
                <div class="addOnSpan">
                  <span :class="counted_class" v-text="$store.getters.remember_me_list.length"></span>
                </div>
                <i class="fas fa-clock"></i>
              </span>
              <span class="column is-12 the-text"> الإجتياز لاحقا </span>
            </p>-->

            <p v-if="$store.getters.user.role == 1" @click="this.window.location.href = '/dashboard'" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-desktop"></i></span>
              <span class="column is-12 the-text"> لوحة التحكم </span>
            </p>

            <p @click="$router.push('/users/'+$store.getters.user.id+'/'+$store.getters.user.name.replace(/[\/\\\s]/g,'-'))" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-cog"></i></span>
              <span class="column is-12 the-text"> إعداداتي </span>
            </p>

            <p @click="$router.push('/instructions-of-use')" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-info-circle"></i></span>
              <span class="column is-12 the-text"> إرشادات </span>
            </p>

            <p @click="$router.push('/logout')" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon"><i class="fas fa-sign-out-alt"></i></span>
              <span class="column is-12 the-text"> خروج </span>
            </p>

          </div>

          <div class="column" @click="closeModal" style="background:none">
          </div>
        </div>

        <!-- login/ register modal -->
        <div v-if="$store.getters.fromState_isAuth == false" class="columns sideHiddenDiv second-one is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile">

            <!-- sign-in button -->
            <span class="button is-default">
              <a href="/login">
              <span> تسجيل الدخول </span>
              <!--<span class="fas fa-envelope"></span>-->
              </a>
            </span>
            <!-- create account -->
            <span class="button is-default">
              <a href="/register">
              <span> إنشاء حساب </span>
              <!--<span class="fas fa-handshake"></span>-->
              </a>
            </span>

            <!-- <p> أو يمكنك إستخدام حسابك في </p>

            Google sign-in button
            <span class="button is-default">
              <span> Google </span>
              <span class="fab fa-google"></span>
            </span>-->

            <!--Facebook sign-in button
            <span class="button is-default is-info" style="color:#fff !important;">
              <span> Facebook </span>
              <span class="fab fa-facebook"></span>
            </span>-->

          </div>

          <div class="column" @click="$store.commit('sideModal',false)" style="background:none">
          </div>
        </div>

        <remember-list-component></remember-list-component>

        <notification-component></notification-component>

      </div>
    </div>
    `,
    data() {
      return {
        sideModal : false,
        open_remember_modal : false,
        counted_value : 0,
        counted_class : 'button is-primary counted-btn',
        categories : [],
        categories_in_dropdown : [],
        dropdown : false,
        modal : false,
      }
    },
    updated(){
      this.notificationsExist();
    },
    mounted(){
      if(this.$store.getters.categories.length == 0) this.getOldCategories();

    },

    methods: {
      notificationsExist(){
        for (let i = 0; i < this.$store.getters.notifications.length; i++) {
          if(this.$store.getters.notifications[i].read_at == null){
            return true;
          }
        }
        return false;
      },

      setLanguage(language){
          document.cookie = "language="+language+"; expires= Infinity";
          this.$store.commit('language',language);
          window.location.replace('/');
      },

      catModal(){
        if(this.modal){
          this.modal = false;
        }else{
          this.modal = true;
        }
      },

      // toByCategoryPage(category_obj){
      //   this.$router.push('/by_category/'+category_obj.id+'/'+category_obj.category);
      // },

      openDropDwon(){
        if(!this.dropdown){
          this.dropdown = true;
        }else{
          this.dropdown = false;
        }
      },

      closeModal(){
        this.$store.commit('open_remember_modal',false);
        this.$store.commit('sideModal',false);
      },

      openSideModal(){
        this.$store.commit('sideModal',true);
        if(this.$store.getters.remember_me_list == 'empty' && this.$store.getters.fromState_isAuth == true){
          this.counted_class = 'button is-primary counted-btn is-loading';

          // get remember list from database
          axios.get('/getRememberList')
            .then(res => {
              this.counted_class = 'button is-primary counted-btn';
              this.$store.commit('remove_remember_me_list');
              this.$store.commit('remove_remember_me_list');
              res.data.forEach(element => {
                this.$store.commit('remember_me_list',{ title : element.title, id: element.id } );
              });
              this.counted_value = this.$store.getters.remember_me_list.length;
            })
            .catch(err => {
              //
            });
        }
      },

      getOldCategories(){
        axios.get('/get_old_categories')
          .then(res => {
              if(res.status == 200){

                if(res.data.length > 6){
                  let categories_in_dropdown = res.data;
                  this.$store.commit('categories',categories_in_dropdown.slice(0,7));
                  this.$store.commit('categories_in_dropdown',res.data.slice(7,res.data.length));
                }else{
                  this.$store.commit('categories',res.data);
                }

              }
          })
          .catch(err => {

          });
      }

    }
  });


// exam card
// show the exam in squared card
Vue.component('exam-card',{
  // exam props contain object of data
  // example :
  // { id:.. ,title:'...', background: '...' ...etc}

  props:['exam'],
  template : `
  <div
    :style="JSON.parse(exam.background).card_back"
    class="card" style="box-shadow: 0 8px 19px #d4d4d4;">

    <div id="cardcontent" class="card-content" :style="JSON.parse(exam.background).background">

      <span class="category-span">
        <router-link :to="{ name: 'category', params: { id: exam.category.id, category: exam.category.category.replace(/[\/\\\s]/g,'-')} }">
            {{ exam.category.category }}
        </router-link>
      </span>
      <h1 ref="titlecustomizing" class="title" dir="rtl" align="center">
         {{ exam.title }}
      </h1>

    </div>
    <footer class="card-footer">
      <p class="card-footer-item item-1">
        <span>
          <a @click="toShowPage(exam.id,exam.title)"> {{ $store.getters.lang.test_now }} </a>
        </span>
      </p>
      <p class="card-footer-item">
        <span>
          <a @click="addToRememberMe({ title : exam.title, id: exam.id })"> {{ $store.getters.lang.remember_me }} </a>
        </span>
      </p>
    </footer>
  </div>
  `,
  data(){
    return {
      edit_exam: false,
      open_exam_dropdown: false,
    }
  },
  methods:{
    toShowPage(id,title){
      title = title.replace(/[\/\\\s]/g,'-');
      this.$router.push('/show/'+id+'/'+title);
    },

    openSideModal(){
        this.$store.commit('sideModal',true);
        if(this.$store.getters.remember_me_list == 'empty'){
          this.counted_class = 'button is-primary counted-btn is-loading';
          axios.get('/getRememberList')
            .then(res => {
              this.counted_class = 'button is-primary counted-btn';
              this.$store.commit('remove_remember_me_list');
              this.$store.commit('remove_remember_me_list');
              res.data.forEach(element => {
                this.$store.commit('remember_me_list',element.title);
              });
              this.counted_value = this.$store.getters.remember_me_list.length;
            })
            .catch(err => {
              //
            });
      }
    },

    addToRememberMe(object){
        this.openSideModal();
          // checking user authentication
          if(this.$store.getters.fromState_isAuth){
            // open modal
            this.$store.commit('open_remember_modal',true);
            // wait until we get the remember list from database
            let interval = setInterval(()=>{
              // checking if we got some data from database
              if(this.$store.getters.remember_me_list != 'empty'){
                // checking if the title exists in the remember list
                if(this.$store.getters.arr_titles_remember_me_list.indexOf(object.title) == -1){
                  // insert a title to database
                  axios.post('/addToRememberList',{
                    title : object.title,
                    exam_id : object.id
                  }).then(res => {
                      if(res.data == true){
                        this.$store.commit('remember_me_list',object);
                        this.$store.commit('arr_titles_remember_me_list',object.title);
                      }else{
                      }
                    })
                    .catch();
                    clearInterval(interval);
                }
              }
            },50);
            // end waiting
          }
    },
  }
});

// modified beufy checkbox
Vue.component('mb-checkbox',{
  props: ['nativeValue','checked'],
  template : `
    <div id="root">
      <div>
      <b-checkbox ref="checkbox" :checked="my_checked"
          :native-value="my_nativeValue" v-model="value">
          <div @click="switchCheckbox()" style="width:100%;">
            <div class="new-check-icon" style="
                position:  absolute;
                font-size: 23px;
                left: 11px;
                top: -4px;
                right:0px;
            ">
              <i ref="square" class="far fa-square"></i>
            </div>
            <slot></slot>
          </div>
      </b-checkbox>
      </div>
    </div>
  `,
  data() {
    return {
      value: null,
      my_checked: this.checked,
      my_nativeValue: this.nativeValue,
    }
  },
  methods: {
    switchCheckbox(){
       if(this.$refs.square.className != 'far fa-square') this.$refs.square.className = 'far fa-square';
       else this.$refs.square.className = 'fas fa-check-square';
       this.$emit('clicked_val',this.value);
    }
  }
});

// statistics's circle
Vue.component('circle-stat',{
  props: ['skill','progress_obj'],
  template : `
  <div id="root">
    <div class="circle-statistics" ref="hola"></div>
      <p dir="rtl" style="
          padding: 5px;
          font-weight:  bold;
          color: #929292;
      ">{{ m_skill.name }}</p>
    </div>
  </div>
  `,
  data(){
    return {
      // my skill : m_skill
      m_skill : this.skill,
      // progress object will be imported in the .vue page
      progress: this.progress_obj
    }
  },
  mounted(){
    // progressbar.js@1.0.0 version is used
    // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

    var cir = new this.progress.Circle(this.$refs.hola, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1000,
      color: '#2b2b2b',
      trailColor: '#eee',
      trailWidth: 1,
      text: this.m_skill.text + '%',
      svgStyle: null,
      text: {
        value: this.m_skill.value*100 + '%',
      }
    });

    // cir.animate(1.0);  // Number from 0.0 to 1.0
    // number of all right answers / the right checked answes
    // ex: 20/ 10 = 2.0

    cir.animate(this.m_skill.value, {
          duration: 1000,
      }, function() {

    });
  }
});


// my amazing button
Vue.component('am-btn',{
  template: `
  <div style="width:100px;" class="am-btn-root">
    <button @click="$emit('click',true)" class="button is-default is-centered m-t-15">
      <slot></slot>
    </button>
    <div class="back-line"></div>
  </div>
  `
});



Vue.component('update-quill-content',{
  props: ['topic','question_title','category','comment_id'],
  template: `
  <div class="columns m-t-15 is-multiline is-mobile">
    <a class="smallUnderlinedText" @click="open_modal='display:block'"> تعديل </a>
    <a class="smallUnderlinedText m-r-10" @click="removeQuestionOrComment($route.params.id)"> حذف </a>

    <div :style="open_modal" class="column is-12 modal">
      <div class="modal-background" @click="open_modal='display:none'"></div>
      <div class="modal-card m-t-30" style="margin:auto">

        <section class="modal-card-body">
          <!-- Content ... -->

          <div class="columns is-multiline">
            <div v-if="this.question_title" class="column is-12 post-label">
              <label> العنوان: </label>
            </div>
            <div v-if="this.question_title" class="column is-12 m-t-0 p-t-0">
              <input placeholder="مثلا: لدي مشكلة في تثبيت البرنامج..." type="text" v-model="title">
            </div>

            <div v-if="this.category" class="column is-12">
              <!-- category -->
              <b-field
                  label="التصنيف">
                  <b-select v-model="selected_category" class="select-cat" placeholder="تصنيف" expanded>
                      <option v-for="cat in this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories)" :value="cat.id">{{ cat.category }}</option>
                  </b-select>
              </b-field>
            </div>
            <div class="column is-12 post-label m-t-25">
              <label> النص: </label>
            </div>
            <div class="column is-12 p-t-0 m-t-0">
              <div ref="QuillArea"></div>
            </div>
          </div>


          <!-- Content ... -->
        </section>
        <footer style="background:#1b1b1b;" dir="ltr" class="modal-card-foot">
          <button ref="submitBTN" class="button is-primary" @click="update()"> تحديث </button>
          <button class="button is-light" @click="open_modal='display:none'"> إلغاء </button>
        </footer>
      </div>
    </div>
  </div>
  `,

  data(){
    return {
      open_modal: 'display:none',
      title: this.question_title,
      selected_category: this.category,
      quill:null,
    }
  },

  mounted(){
    // hljs.initHighlightingOnLoad();
    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax: true,
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold'], //'italic', 'underline'
          ['code-block','blockquote'],
          ['link']
        ],
      },
      theme: 'snow',
      formats : [
        'background',
        'bold',
        'color',
        'font',
        'code',
        'italic',
        'link',
        'size',
        'strike',
        'script',
        'underline',
        'blockquote',
        'header',
        'indent',
        'list',
        'align',
        'direction',
        'code-block',
        'formula'
        // 'image'
        // 'video'
      ]
    });
    this.quill.setContents(JSON.parse(this.topic));
  },
  watch: {
    open_modal(val){
      if(val == 'display:none'){
        this.quill.setContents(JSON.parse(this.topic));
      }
    }
  },
  methods: {
    removeQuestionOrComment(id){
      if(this.question_title && this.category){
        this.$dialog.confirm({
            title: 'عملية حذف',
            message: ' هل حقا تود إجراء عملية الحذف ؟ ',
            confirmText: 'حذف',
            cancelText:'إلغاء ',
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_question/'+id)
              .then(res => {
                if(res.data == 'true') {
                  this.$toast.open({
                      duration: 5000,
                      message: `تم حذف السؤال`,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                  this.$route.push('/');
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: ` لا يمكنك حذف هذا المنشور، راجع إرشادات الإستخدام `,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }
              })
              .catch(err => {

              });
            }
        });
      }else{
        this.$dialog.confirm({
            title: 'عملية حذف',
            message: ' هل حقا تود إجراء عملية الحذف ؟ ',
            confirmText: 'حذف',
            cancelText:'إلغاء ',
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_comment/'+this.comment_id)
              .then(res => {
                if(res.data == 'true') {
                  this.$toast.open({
                      duration: 5000,
                      message: ` تم حذف هذا التعليق `,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                  document.getElementById('comment'+this.comment_id).style.display= 'none';
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: ` لا يمكنك حذف هذا المنشور، راجع إرشادات الإستخدام `,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }
              })
              .catch(err => {

              });
            }
        });
      }
    },

    update(){
      this.$refs.submitBTN.className='button is-primary is-loading';
      // question case
      if(this.question_title && this.category){
        axios.post('/update_a_question/'+this.$route.params.id,{
          user_id: this.$store.getters.user.id,
          title : this.title,
          category_id : this.selected_category,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              title : this.title,
              category : this.selected_category,
              topic: JSON.stringify(this.quill.getContents())
            });
            // window.location.reload();
          }
        })
        .catch(err => {
          this.$refs.submitBTN.className='button is-primary';
          this.$snackbar.open({
              message: ' تأكد من ملئ جميع البيانات ',
              type: 'is-danger',
              position: 'is-bottom',
              actionText: 'إغلاق',
              indefinite: true,
              onAction: () => {

              }
          });
        });

      // comment case
      }else{
        axios.post('/update_a_comment/'+this.comment_id,{
          user_id: this.$store.getters.user.id,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              topic: JSON.stringify(this.quill.getContents())
            });
          }
        })
        .catch(err => {
        });
      }

    }
  }

});

Vue.component('Quill-bubble-show',{
  props: ['topic'],
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null
    }
  },

  watch: {
    topic:function(val){
      if(this.quill.getText().length){
        this.quill.setContents(JSON.parse(val));
      }
    }
  },

  mounted(){
    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'bubble',
      readOnly: true,
      modules: {
        syntax: true
      },
    });
    this.quill.setContents(JSON.parse(this.topic));
    // call computed property (ttopic) for setting contents to quill
    // this.ttopic;

  }
});

Vue.component('Quill-Snow-Editor',{
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null,
    }
  },

  mounted(){
    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'snow',
      readOnly: true,
      modules: {
        syntax: true,              // Include syntax module
        toolbar: [['code-block']]  // Include button in toolbar
      },
      formats : [
        'background',
        'bold',
        'color',
        'font',
        'code',
        'italic',
        'link',
        'size',
        'strike',
        'script',
        'underline',
        'blockquote',
        'header',
        'indent',
        'list',
        'align',
        'direction',
        'code-block',
        'formula'
        // 'image'
        // 'video'
      ]
    });

    let interval = setInterval(()=>{
      if(this.topic) {
        this.quill.setContents(JSON.parse(this.topic));
        clearInterval(interval);
      }
    },10);
  }

});

Vue.component('reply-box',{
  props: ['comment','questionId','question_title'],
  template : `
  <!-- reply  textarea-->
  <div class="column" v-show="comment.open_reply == true && comment.is_closed == 0">

    <div class="columns is-multiline m-b-15">
      <b-message v-if="$store.getters.user.id == undefined" type="is-success" class="column is-12 nextCaseWarning" dir="rtl">
        <a class="p-r-20" href="/register"> {{ $store.getters.lang.register_to_comment_message }} </a>
      </b-message>

      <div v-show="$store.getters.user.id" class="column is-12 loginDiv comment reply-box">
        <div class="columns is-multiline">
          <div class="column is-12 comment-header">

          <img :src="'https://www.gravatar.com/avatar/'+$store.getters.user.profile_img+'?d=https://i.suar.me/varoL/l'">
              <div v-if="$store.getters.user.id">
                <router-link :to="{ name: 'user', params: { id: $store.getters.user.id, name: $store.getters.user.name.replace(/[\/\\\s]/g,'-') }}" > {{ $store.getters.user.name }} </router-link>
              </div>
          </div>
          <div class="column is-12 p-t-0 m-t-0">
            <div ref="QuillArea"></div>
          </div>
          <div class="column is-12">
            <button :class="btn_class" @click="sendToDB()">تعليق</button>
          </div>

        </div>
      </div>

    </div>

  </div>
  `,

  mounted(){

    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax : true,
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold'], //'italic', 'underline'
          ['code-block','blockquote'],
          ['link'],
        ]
      },
      theme: 'snow',
      //readOnly: true
    });

    // this.quill.focus();
  },

  data(){
    return {
      // quill
      quill: null,
      question_id: this.questionId,
      comments: [],
      new_reply : {
        topic: '',
        question_id : 0,
        is_closed: 0,
        user_id : this.comment.user.id,
        question_title: this.question_title,
        question_id: this.questionId
      },
      btn_class: 'button is-primary',
      window,
      origin_path: window.location.origin
    }
  },

  methods:{

    // send comment to database
    sendToDB(){
      if(this.quill.getText().length < 2){
          this.$snackbar.open({
              message: ' لم تكتب شيئا ',
              type: 'is-danger',
              position: 'is-bottom',
              actionText: 'إغلاق',
              indefinite: true,
              onAction: () => {
                  this.$toast.close();
              }
          });
          this.btn_class = 'button is-primary';
          return;
      }
      this.btn_class = 'button is-primary is-loading';
      this.new_reply.question_title = this.question_title;
      this.new_reply.topic = JSON.stringify(this.quill.getContents());
      this.new_reply.topic_text = this.quill.getText();
      this.new_reply.parent_comment_id = this.comment.id;
      this.new_reply.question_id = this.questionId;
      axios.post('/post_a_comment',this.new_reply)
      .then(res => {
        this.$emit('commingresult',{
          id: res.data,
          user: this.$store.getters.user,
          user_id: this.$store.getters.user.id,
          open_reply: false,
          topic: JSON.stringify(this.quill.getContents()),
          is_closed: 0,
          likes: 0,
          replies: 0,
          question_id: this.question_id,
          parent_comment_id: this.comment.id,
          question_id: this.questionId,
          question_title: this.question_title,
          created_at: Date.now(),
          updated_at: Date.now()
        });

        this.quill.setContents([]);
        this.btn_class = 'button is-primary';
        //////////////
      })
      .catch(err => {
        this.btn_class = 'button is-primary';
        this.$snackbar.open({
            message: ' لم تكتب شيئا ',
            type: 'is-danger',
            position: 'is-bottom',
            actionText: 'إغلاق',
            indefinite: true,
            onAction: () => {
                this.$toast.close();
            }
        });
      });
    },

  }
});

// Below, we are making infinit loading for comments and it's replies

// first reply
Vue.component('f-reply',{
  props: ['commentId','questionId','counter','question_title','newcomment'],
  template : `
  <div ref="replies">
    <!-- loading spinner -->
    <button v-if="all_comments.length == 0" class="button chain-loading is-large is-loading"></button>

    <div v-if="all_comments.length">

      <!-- comment reply -->
      <div v-for="comment in all_comments" :id="'comment'+comment.id">
        <div class="column is-12 loginDiv comment">
          <div class="columns is-multiline">

          <!-- for tablet width and heigher -->
          <div v-if="window.innerWidth >= 1024" class="column is-12 comment-header is-relative">
              <like-btn target_type="comment" :user_id="comment.user.id" :target_id="comment.id" :value="comment.likes"></like-btn>

              <img :src="'https://www.gravatar.com/avatar/'+comment.user.profile_img+'?d=https://i.suar.me/varoL/l'">
              <user-comment-time :question_object="comment" hide_comments="true" hide_is_closed="true"></user-comment-time>
          </div>
          <!-- for mobile, tablet -->
          <div v-if="window.innerWidth < 768" class="column is-12" style="border-bottom: 1px solid #eee;margin-bottom: 10px;">
              <div class="columns is-multiline is-mobile">
                  <div class="column is-2 is-small-thing">
                    <like-btn-2 target_type="comment" :user_id="comment.user.id" :target_id="comment.id" :value="comment.likes"></like-btn-2>
                  </div>
                  <img class="img-is-50" :src="'https://www.gravatar.com/avatar/'+comment.user.profile_img+'?d=https://i.suar.me/varoL/l'">
                  <user-comment-time class="m-t-10" hide_time="true" :question_object="comment" hide_comments="true" hide_is_closed="true"></user-comment-time>
              </div>
          </div>


            <div class="column is-12">

              <Quill-bubble-show :topic="comment.topic"></Quill-bubble-show>

              <update-quill-content @changed="changecommentdata($event,comment.id)" v-if="$store.getters.user != undefined && $store.getters.user.id == comment.user.id" :topic="comment.topic" :comment_id="comment.id"></update-quill-content>


              <!-- open the reply box -->
              <div class="comment-footer column is-12">
                <!-- <close-open-target :user_id="comment.user.id" target_type="comment" :target_id="comment.id" :is_closed="comment.is_closed == 1"></close-open-target>-->
                <button v-if="$store.getters.user.id>0" :disabled="comment.is_closed == 1" @click="comment.open_reply = true" class="button is-default is-small">
                  <i class="fas fa-plus p-l-5"></i> إضافة رد
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="column chain" ref="comment">

          <!-- reply  textarea-->
          <reply-box @commingresult="addcomment($event,comment.id);comment.replies++" :question_title="question_title" :questionId="question_id" :comment="comment"></reply-box>

          <!-- /// MOBILE /// -->
          <f-reply v-if="same_counter != 0 && comment.replies > 0 && window.innerWidth <= 768" :newcomment="new_comment" :question_title="question_title" :counter="same_counter+1" :questionId="question_id" :commentId="comment.id"></f-reply>
          <!-- /// MOBILE /// -->
          <!-- show more replies -->
          <router-link v-if="same_counter == 0 && comment.replies && window.innerWidth <= 768" :id="comment.id" class="smallUnderlinedText" :to="{ name: 'fullReplies', params : { id : $route.params.id, title : question_title.replace(/[\/\\\s]/g,'-'), commentid : comment.id } }">
           ( {{ comment.replies }} )  عرض باقي الردود
            <i class="fas fa-caret-left"></i>
          </router-link>

          <!-- /// DESKTOP /// -->
          <f-reply v-if="same_counter != 4 && comment.replies > 0 && window.innerWidth >= 1024" :newcomment="new_comment" :question_title="question_title" :counter="same_counter+1" :questionId="question_id" :commentId="comment.id"></f-reply>
          <!-- /// DESKTOP /// -->
          <!-- show more replies -->
          <router-link v-if="same_counter == 4 && comment.replies && window.innerWidth >= 1024" :id="comment.id" class="smallUnderlinedText" :to="{ name: 'fullReplies', params : { id : $route.params.id, title : question_title.replace(/[\/\\\s]/g,'-'), commentid : comment.id } }">
           ( {{ comment.replies }} )  عرض باقي الردود
            <i class="fas fa-caret-left"></i>
          </router-link>

        </div>
      </div>

    </div>
  </div>
  `,
  data(){
    return {
      origin_path: window.location.origin,
      moment : moment,
      new_comment: null,
      all_comments : [],
      comment_id: this.commentId,
      question_id: this.questionId,
      same_counter: this.counter,
      window
      // question_title : this.question_title,
    }
  },

  mounted(){
    this.getComments();
  },

  methods:{

    changecommentdata(object,id){
      for (var i = 0; i < this.all_comments.length; i++) {
        if(this.all_comments[i].id == id){
          this.all_comments[i].topic = object.topic;
        }
      }
    },

    addcomment(object){
      if(object.parent_comment_id != this.commentId) this.new_comment = object;//this.getNewComment(object);
      else{
        this.all_comments.push(object);
        this.$router.push(this.$route.path+'#comment'+object.id);
      }
    },

    getNewComment(object){
      this.new_comment = object;
    },

    // get all comments
    getComments(){
      axios.get(`/comments/${this.commentId}/${this.questionId}`)
        .then(res => {
          // make an option to open/close the reply box
          for (var i = 0; i < res.data.length; i++) {
            res.data[i].open_reply = false;
          }

          this.all_comments = res.data;
          this.comments_ready = true;
          NProgress.done();
        })
        .catch(err => {

        });
    }
  },

  watch: {
    newcomment(obj){
      this.all_comments.push(obj);
      this.$router.push(this.$route.path+'#comment'+obj.id);
    }
  }
});


Vue.component('side-exams',{
  props: ['category_id'],
  template: `
  <div class="column is-12">

      <div v-if="side_exams.length>0" class="columns is-multiline">
        <div class="column is-full p-0 m-b-15" v-for="exam in side_exams">
          <!-- show the exam in squared card -->
          <exam-card :exam="exam"></exam-card>
        </div>

        <!--<div ref="loading" class="column is-12 container infinite-div">
          <infinite-loading spinner="waveDots" @infinite="getSideExams"></infinite-loading>
          <span slot="no-results">
            <span v-if="no_more_data" slot="no-results">
              <div class="columns">
                <div class="column is-5"></div>
                <div class="column"> <span class="sectionLebel" font-size="25px;"> لا يوجد إختبارات </span> </div>
                <div class="column is-5"></div>
              </div>
            </span>
          </span>
        </div>-->
      </div>

  </div>
  `,

  // update(){
  //   if(this.InfiniteLoading == null) this.InfiniteLoading = require('vue-infinite-loading');
  //
  //   if(this.side_exams.length == 0) {
  //       window.scrollTo(0, 0);
  //       // call it after load the exam
  //       if(this.category != null) this.getSideExams();
  //   }
  // },

  mounted(){
    // if(this.InfiniteLoading == null) this.InfiniteLoading = require('vue-infinite-loading');
    //
    // if(this.side_exams.length == 0) {
    //     window.scrollTo(0, 0);
    //     // call it after load the exam
    //     if(this.category != null) this.getSideExams();
    // }
    this.getSideExams();
  },

  data(){
    return {
      side_exams: [],
      // InfiniteLoading: null,
      no_more_data: false,
    }
  },

  methods: {
    getSideExams($state){

      axios.get('/exams_where_category/'+this.category_id+'/'+this.side_exams.length+'/10')
        .then(response => {

          if(!response.data.length){
            // for show my custom "no more data" message
            this.no_more_data = true;
            // $state.complete();

            // for hiding the "no more data :)" message of the infinite loading
            this.$refs.loading.childNodes[0].style.display = "none";
          }else{
            this.side_exams = this.side_exams.concat(response.data);
            // $state.loaded();
          }
        }).catch(err =>{
        });
    },

  },

});

// component for similar questions
Vue.component('side-section-random-questions',{
  template: `
    <div class="loginDiv m-b-20 column is-12 m-b-10">
      <h5> <i class="far fa-star"></i> الأفضل لهذا الأسبوع </h5>
      <h1></h1>
      <ul ref="ul" class="ul-similar-question">
        <li v-for="question in questions">
            <div class="columns is-mobile is-tablet ">
              <!--<div class="column is-2 m-t-5">
                <span v-if="question.is_answered != 0" class="button is-success">
                    <i class="fa fa-check"></i>
                </span>
                <span v-if="question.is_answered == 0" class="button is-grey"><i class="fa fa-question"></i>
                </span>
              </div>-->
              <div class="column is-2 m-t-15 m-b-15 m-r-10">
               <span class="columns is-multiline is-mobile is-tablet right-span-in-similar-q">
                  <span class="column text-center is-12 p-b-0">
                    <i class="fas fa-comment-alt"> </i>
                  </span>
                  <span class="column is-12 text-center p-t-0"> {{ question.comments }} </span>
                </span>
              </div>
              <div class="column is-10">
                <h3 style="overflow:hidden">
                  <router-link :to="{ name: 'showQuestion', params : { id: question.id, title: question.title.replace(/[\/\\\s]/g,'-') }  }">{{ question.title }}</router-link>
                </h3>
              </div>
            </div>
        </li>
      </ul>
    </div>
  `,

  data(){
    return {
      questions: []
    }
  },

  mounted(){
    this.getSimilarQuestions();
  },

  methods: {
      getSimilarQuestions(){
        axios.get('/get_random_questions/'+this.questions.length+'/'+10)
          .then(res => {
            if(!res.data.length){
              this.$refs.ul.innerHTML = '<li class="text-center" style="border-bottom:none;"> لا يوجد </li>'
            }
            this.questions = this.questions.concat(res.data);
          })
          .catch(err => {

          });
      }
  }

});

// component for questions on the same category
Vue.component('same-category-question',{
  props : ['category_id'],
  template: `
    <div class="column is-12 latest-commented-posts">
      <ul>
        <li v-for="question in questions">
          <h4 style="overflow:hidden">
            <router-link :to="{ name: 'showQuestion', params : { id: question.id, title: question.title.replace(/[\/\\\s]/g,'-') }  }">{{ question.title }}</router-link>
          </h4>
        </li>
      </ul>
    </div>
  `,

  data(){
    return {
      questions: []
    }
  },

  mounted(){
    this.getSimilarQuestions();
  },

  methods: {
      getSimilarQuestions(){
        axios.get('/get_questions_in_the_same_category/'+this.category_id)
          .then(res => {
            if(!res.data.length){
              this.$refs.ul.innerHTML = '<li class="text-center" style="border-bottom:none;"> لا يوجد </li>'
            }
            this.questions = res.data;
          })
          .catch(err => {

          });
      }
  }

});

// component for questions on the same category
Vue.component('common-questions',{
  template: `
    <div class="column is-12 latest-commented-posts">
      <ul>
        <li v-for="question in questions">
          <h1 style="font-size:15px;overflow:hidden">
            <router-link :to="{ name: 'showQuestion', params : { id: question.id, title: question.title.replace(/[\/\\\s]/g,'-') }  }">{{ question.title }}</router-link>
          </h1>
        </li>
      </ul>
    </div>
  `,

  data(){
    return {
      questions: []
    }
  },

  mounted(){
    this.getCommonQuestions();
  },

  methods: {
      getCommonQuestions(){
        axios.get('/get_common_questions/')
          .then(res => {
            if(!res.data.length){
              this.$refs.ul.innerHTML = '<li class="text-center" style="border-bottom:none;"> لا يوجد </li>'
            }
            this.questions = res.data;
          })
          .catch(err => {

          });
      }
  }

});


// category selector component

Vue.component('category-selector',{
  props: ['label','size','placeholder','v-model'],
  template: `
  <div class="field">
    <div class="control">
      <b-field
          :label="this.label">
          <b-select @input="emitValue" v-model="selected_category" :size="this.size" class="select-cat" :placeholder="this.placeholder" expanded>
              <option v-for="cat in this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories)" :value="cat.id">{{ cat.category }}</option>
          </b-select>
      </b-field>
    </div>
  </div>
  `,

  data(){
    return {
      // categories selector
      categories: this.$store.getters.categories_in_dropdown.concat(this.$store.getters.categories),
      selected_category: null,
      // watch_selected_category : this.selected_category,
    }
  },

  methods : {
    emitValue(){
      this.$emit('input',this.selected_category);
    }
  }

});


// show all categories in the bottom footer
// Vue.component('categories-in-footer',{
//   template : `
//     <div class="root footer-categories">
//       <div v-for="cat in $store.getters.categories_in_dropdown.concat($store.getters.categories)">
//         <span>
//           <router-link to="#">{{ cat.category }}</router-link>
//         </span>
//       </div>
//     </div>
//   `,
// });


Vue.component('user-comment-time',{
  props: ['question_object','hide_comments','hide_user','hide_time','hide_is_closed'],
  template: `
    <div class="root user-comment-time">
      <div>
        <div v-if="this.hide_comments">
          <router-link :to="{ name: 'user', params: { id: this.question_object.user.id, name: this.question_object.user.name.replace(/[\/\\\s]/g,'-')  } }"> {{ this.question_object.user.name }} </router-link>
        </div>
        <div style="float:right" v-else>
          <router-link :to="{ name: 'user', params: { id: this.question_object.user.id, name: this.question_object.user.name.replace(/[\/\\\s]/g,'-')  } }"> {{ this.question_object.user.name }} </router-link>
        </div>
        <div v-if="!this.hide_comments">
          <i class="far fa-comment"></i> {{ this.question_object.comments }} تعليق
        </div>
        <div v-if="!this.hide_time">
          <i class="far fa-clock"></i> {{ moment.utc(this.question_object.created_at).fromNow() }}
        </div>

        <router-link v-if="this.question_object.category && this.question_object.category.id" :to="{ name: 'category', params: { id: this.question_object.category.id, category: this.question_object.category.category.replace(/[\/\\\s]/g,'-') } }">
          <div class="category">{{ this.question_object.category.category }}</div>
        </router-link>

        <div v-if="$store.getters.user.role != undefined && $store.getters.user.role == 1 ">
          <a class="smallUnderlinedText" @click="removeQuestion(question_id)"> حذف </a>
        </div>
      </div>
    </div>
  `,

  data(){
    return {
        moment,
        question_id: this.question_object.id
    }
  },

  methods: {

    removeQuestion(id){
      this.$dialog.confirm({
          title: 'عملية حذف',
          message: ' هل حقا تود إجراء عملية الحذف ؟ ',
          confirmText: 'حذف',
          cancelText:'إلغاء ',
          type: 'is-danger',
          hasIcon: false,
          onConfirm: () => {
            axios.post('/remove_question/'+id)
            .then(res => {
              if(res.data == 'true') {
                this.$emit('deletequestion',id);
                this.$toast.open({
                    duration: 5000,
                    message: `تم حذف السؤال`,
                    position: 'is-bottom',
                    type: 'is-dark'
                });
              }else if(res.data == '401-forbidden'){
                this.$toast.open({
                    duration: 5000,
                    message: ` لا يمكنك حذف هذا المنشور، راجع إرشادات الإستخدام `,
                    position: 'is-bottom',
                    type: 'is-dark'
                });
              }
            })
            .catch(err => {

            });
          }
      })
    },

  }
});


Vue.component('show-right-or-false-answers',{
  props: ['the_answers','the_questions','mode','all_the_answers'],
  template : `
  <div v-if="questions.length > 0" class="column is-12 root">
    <div class="columns is-multiline">
      <ul v-if="mode == 'right_answers'" class="column is-12 rightRes rtl">
          <h3 class="h3_right_answers"><i class="fa fa-check-circle"></i> الإجابات الصحيحة </h3>
          <li v-for="answer in answers">

              <!-- if it's a simple question -->
              <h4 v-if="questions[answer.question_id].question.charAt(0) != '{' " class="result_ques ">- {{ questions[answer.question_id].question }}</h4>

              <!-- if it's a quill question -->
              <div v-if="questions[answer.question_id].question.charAt(0) == '{'" class="column is-12 p-0">
                <Quill-bubble-show :topic="questions[answer.question_id].question"></Quill-bubble-show>
              </div>

              <!-- your answer -->
              <span class="result_ans p-r-20"><i class="fas fa-check ans_icon"></i> {{ answer.ans }}</span>

              <!-- the right answer -->
              <div v-for="ans1 in all_answers[answer.question_id]">
                  <div v-if="ans1[0] != answer.ans">
                      <span class="result_ans p-r-20" v-if="ans1[1]">
                          <b-tooltip label="إجابة صحيحة، لم تخترها"
                          position="is-left"
                          type="is-dark"
                          size="is-large"
                          animated>
                              <i class="fas fa-info-circle ans_icon"></i>
                          </b-tooltip>
                          {{ ans1[0] }} <br></span>
                  </div>
              </div>
          </li>
      </ul>

      <ul v-if="mode != 'right_answers'" class="column is-12 rightRes falseRes rtl">
          <h3 class="h3_right_answers false_answer"><i class="fas fa-times-circle"></i> الإجابات الخاطئة</h3>
          <li v-for="answer in answers">

              <!-- if it's a simple question -->
              <h4 v-if="questions[answer.question_id].question.charAt(0) != '{' " class="result_ques ">- {{ questions[answer.question_id].question }}</h4>

              <!-- if it's a quill question -->
              <div v-if="questions[answer.question_id].question.charAt(0) == '{'" class="column is-12 p-0">
                <Quill-bubble-show :topic="questions[answer.question_id].question"></Quill-bubble-show>
              </div>

              <!-- your answer -->
              <span class="result_ans line-through p-r-20"><i class="fas fa-times ans_icon"></i> {{ answer.ans }}</span>
              <br>
              <!-- the right answer -->
              <div v-for="ans in all_answers[answer.question_id]">
                  <span class="result_ans p-r-20" v-if="ans[1]"><i class="fas fa-check ans_icon"></i> {{ ans[0] }} <br></span>
              </div>

          </li>
      </ul>
      <br>
    </div>
  </div>
  `,

  data(){
    return {
      answers : this.the_answers,
      all_answers: this.all_the_answers,
      questions : this.the_questions
    }
  }
});


Vue.component('follow-button',{
  template: `
    <div class="root">
      <b-tooltip
          position="is-bottom"
          type="is-dark"
          label="بالضغط، سيتم تنبيهك بكل الردود المباشرة لهذا التعليق دون التعليقات الفرعية."
          multilined
          animated>
              <button class="button is-info is-small"> <i class="fas fa-bell"></i> </button>
      </b-tooltip>
    </div>
  `
});

Vue.component('hamza-toast',{
  props: ['text','LightButton','PrimaryButton','done'],
  template: `
  <div :style="style" class="hamza-toast root loginDiv columns is-multiline is-mobile">
    <div class="column is-12">
      <p>{{ this.text }}</p>
    </div>
    <hr>
    <div class="column is-12" style="text-align:center;">
      <span class="button is-light m-r-5" @click="cancel()"> {{ this.LightButton }} </span>
      <span ref="submit" class="button is-primary" @click="toggle();$emit('click',true)"> {{ this.PrimaryButton }} </span>
    </div>
  </div>
  `,
  data(){
    return {
      style : 'bottom:-500px'
    }
  },

  methods: {
    cancel(){
        this.$emit('cancel',true);
        this.$refs.submit.className = 'button is-primary';
        this.style = 'bottom:-500px;transition:0.5s';
    },
    toggle(){
      this.$refs.submit.className += ' is-loading';
    }
  },

  watch: {
    done(value){
      if(value) {
        this.$refs.submit.className = 'button is-primary';
      }
    }
  }
});


Vue.component('like-btn',{
  props: ['target_type','target_id','user_id','value'],
  template: `
    <div class="like-button columns is-multiline">
        <i class="column is-12 fas fa-angle-up" @click=" data_to_send.action = 'like';sendToDB()"></i>
        <span ref="like_btn" class="column is-12" >
          {{ formatString(get_value) }}
        </span>
        <i class="column is-12 fas fa-angle-down" @click=" data_to_send.action = 'dislike';sendToDB()"></i>
    </div>
  `,
  data(){
    return {
        get_value: this.value,
        data_to_send : {
          user_id : this.user_id,
          action: null,
          target_type: this.target_type,
          target_id: this.target_id
        }
    }
  },
  mounted(){
    if(this.$refs.like_btn.innerText.length >= 3) this.$refs.like_btn.style='font-size: 15px !important;';
  },
  methods: {
    sendToDB(){
      if(!this.$store.getters.user.id){
        this.$snackbar.open({
            message: ' يتوجب عليك الإنضمام للموقع ',
            type: 'is-warning',
            position: 'is-bottom',
            actionText: 'إغلاق',
            indefinite: true,
            onAction: () => {
                this.$toast.close();
            }
        });
        return;
      }else if(this.$store.getters.user.id == this.user_id){
        this.$snackbar.open({
            message: ' لا يمكنك تقييم محتوى خاص بك ',
            type: 'is-warning',
            position: 'is-bottom',
            actionText: 'إغلاق',
            indefinite: true,
            onAction: () => {
                this.$toast.close();
            }
        })
        return;
      }

      axios.post('/like-dislike',this.data_to_send)
        .then(res => {
          if(res.data == '401-dislike'){
            // check for authorization
            if(this.data_to_send.action == 'dislike' && this.$store.getters.user.reputation < 250){
              this.$snackbar.open({
                  message: ' رصيد سمعتك: '+this.$store.getters.user.reputation+' لم يصل حتى الآن لـ 250 وحدة ',
                  type: 'is-warning',
                  position: 'is-bottom',
                  actionText: 'إغلاق',
                  indefinite: true,
                  onAction: () => {
                      // this.$snackbar.;
                  }
              })
            }
          }else if(res.data == '401-owner'){ /**/ }
          else{
            if(!res.data && res.data !== 0) res.data = this.$refs.like_btn.innerText;
            this.$refs.like_btn.innerText = res.data;
          }
        }).catch(err => {

        });
    },

    formatString(value){
      if(value == 0) return 0;
      if(value < 1000) return value;

      value = ''+value+'';
      let new_value = '';
      for (var i = 0; i < value.length; i++) {
        if((i%3)== 0 && i != value.length-1) new_value += value.charAt(i)+',';
        else new_value += value.charAt(i);
      }
      return new_value;
    },
  }
});

Vue.component('like-btn-2',{
  props: ['target_type','target_id','user_id','value'],
  template: `
  <div class="columns is-multiline NewLikeButton">
      <div style="cursor:pointer" class="column is-12 text-center p-b-5" @click=" data_to_send.action = 'like';sendToDB()">
        <i class="fas fa-angle-up"></i>
      </div>
      <div ref="like_btn" class="column is-12  text-center is-middleOne">
        {{ formatString(get_value) }}
      </div>
      <div style="cursor:pointer" class="column is-12  text-center p-t-5" @click=" data_to_send.action = 'dislike';sendToDB()">
        <i class="fas fa-angle-down"></i>
      </div>
  </div>
  `,
  data(){
    return {
        get_value: this.value,
        data_to_send : {
          user_id : this.user_id,
          action: null,
          target_type: this.target_type,
          target_id: this.target_id
        }
    }
  },
  mounted(){
    if(this.$refs.like_btn.innerText.length >= 3) this.$refs.like_btn.style='font-size: 15px !important;';
  },
  methods: {
    sendToDB(){
      if(!this.$store.getters.user.id){
        this.$snackbar.open({
            message: ' يتوجب عليك الإنضمام للموقع ',
            type: 'is-warning',
            position: 'is-bottom',
            actionText: 'إغلاق',
            indefinite: true,
            onAction: () => {
                this.$toast.close();
            }
        });
        return;
      }else if(this.$store.getters.user.id == this.user_id){
        this.$snackbar.open({
            message: ' لا يمكنك تقييم محتوى خاص بك ',
            type: 'is-warning',
            position: 'is-bottom',
            actionText: 'إغلاق',
            indefinite: true,
            onAction: () => {
                // this.$snackbar.;
            }
        })
      }
      axios.post('/like-dislike',this.data_to_send)
        .then(res => {
          if(res.data == '401-dislike'){
            // check for authorization
            if(this.data_to_send.action == 'dislike' && this.$store.getters.user.reputation < 250){
              this.$snackbar.open({
                  message: ' رصيد سمعتك: '+this.$store.getters.user.reputation+' لم يصل حتى الآن لـ 250 وحدة ',
                  type: 'is-warning',
                  position: 'is-bottom',
                  actionText: 'إغلاق',
                  indefinite: true,
                  onAction: () => {
                      // this.$snackbar.;
                  }
              })
            }
          }else if(res.data == '401-owner'){ /**/ }
          else{
            if(!res.data && res.data !== 0) res.data = this.$refs.like_btn.innerText;
            this.$refs.like_btn.innerText = res.data;
          }
        }).catch(err => {

        });
    },

    formatString(value){
      if(value == 0) return 0;
      if(value < 1000) return value;

      value = ''+value+'';
      let new_value = '';
      for (var i = 0; i < value.length; i++) {
        if((i%3)== 0 && i != value.length-1) new_value += value.charAt(i)+',';
        else new_value += value.charAt(i);
      }
      return new_value;
    },
  }
});


Vue.component('close-open-target',{
  /*
  * target_type : question, comment (string)
  * target_id : (integer)
  * is_closed : (boolean)
  */
  props: ['target_type','target_id','is_closed','user_id'],
  template : `
  <button v-if="$store.getters.user && $store.getters.user.id == this.user_id" style="padding:0px;margin:0px;background:none;border:none;">
    <span v-if="closed" ref="close" @click="open()" class="button is-info is-small" style="top:-1px;">
       <i class="fas fa-minus-circle p-l-5"></i> مغلق
    </span>
    <span v-if="!closed" ref="closed" @click="close()" class="button is-default is-small" style="top:-1px;">
       <i class="fas fa-ban p-l-5"></i> غلق
    </span>
  </button>
  `,
  data(){
    return {
      closed : this.is_closed,
    }
  },
  methods: {
    close(){
      let className = this.$refs.closed.className;
      this.$refs.closed.className += ' is-loading';
      axios.post('/close-target',{
        target_type : this.target_type,
        target_id: this.target_id,
        user_id: this.$store.getters.user.id
      })
      .then(res => {
          if(res.data){
            this.$refs.closed.className = className;
            this.closed = true;
          }
      })
      .catch(err => {
        //
      });
    },

    open(){
      let className = this.$refs.close.className;
      this.$refs.close.className += ' is-loading';
      axios.post('/open-target',{
        target_type : this.target_type,
        target_id: this.target_id,
        user_id: this.$store.getters.user.id
      })
      .then(res => {
          if(res.data){
            this.$refs.close.className = className;
            this.close = false;
          }
      })
      .catch(err => {
        //
      });
    }
  }
});

Vue.component('divider',{
  template: `
  <div class="divider column is-12 is-relative">
  	<span class="button is-light">
      <slot></slot>
    </span>
    <hr>
  </div>
  `
});

Vue.component('maxim',{
  template: `
  <div v-if="maxim[4] && maxim[4].name" class="maxim column p-0">
      <div class="loginDiv text-center m-b-15">

        <p>{{ maxim[0] }}</p>

        <a> {{ maxim[4].name }} </a>
        <div class="maxim-footer">
            <span> {{ maxim[3] | formatLikes }} </span>
            <!-- <input v-model="check" :value="maxim[5]" type="checkbox">-->
            <!-- class="b-checkbox checkbox" -->
            <label tabindex="0" class="b-checkbox checkbox" checked="checked">
              <input v-model="check" type="checkbox">
              <span class="check"></span>
            </label>
            <i v-if="$store.getters.user.role == 1" class="fas fa-plus" @click="cardModal()"></i>
        </div>
        <div ref="notAuthAlert" style="display:none" class="notAuthAlert is-success">
          <a href="/register" style="color:#2b2b2b">
            {{ $store.getters.lang.register_to_post_maxim_message }}
          </a>
        </div>
      </div>
  </div>
  `,

  data(){
    return {
      topic: null,
      check: undefined,
      maxim: [],
      modal: {
        template: `
            <div class="dialog modal is-active maxim-dialog">
              <div class="modal-card loginDiv animation-content" style="width: 105%;">
                <header class="modal-card-head">
                  <p class="modal-card-title"> أمتعنا بحكمة جميلة </p>
                </header>
                  <section class="modal-card-body">
                    <div class="media"><!---->
                      <div class="media-content">
                        <textarea style="resize:vertical" ref="maximTextarea" v-model="topic"></textarea>
                      </div>
                    </div>
                  </section>
                <footer class="modal-card-foot">
                  <button @click="$parent.close()" class="button is-light">
                    إلغاء
                  </button>
                  <button ref="submitBTN" @click="sendToDB()" class="button is-primary">
                    حفظ
                  </button>
                </footer>
              </div>
            </div>
        `,

        data(){
          return {
            topic: null
          }
        },

        methods: {
          sendToDB(){
            this.$refs.submitBTN.className='button is-primary is-loading';
            axios.post('/save_maxim',{
              topic: this.topic
            }).then(res => {
              if(res.status == 200) {
                this.$refs.submitBTN.className='button is-primary';
                // window.location.reload();
              }
            })
            .catch(err => {
              document.alert(' حدث خطأ ');
            });

          }
        }
      }
    }
  },
  mounted(){
    this.getRandomMaxim();
  },
  filters:{
    formatLikes: (val) => {
      if(val == 0) return 0;
      if(val < 1000) return val;

      val = ''+val+'';
      let new_value = '';
      for (var i = 0; i < val.length; i++) {
        if((i%3)== 0 && i != val.length-1) new_value += val.charAt(i)+',';
        else new_value += val.charAt(i);
      }
      return new_value;
    }
  },
  methods: {
    getRandomMaxim(){
      axios.get('/get_random_maxim')
        .then(res => {
          this.maxim = res.data;
          if(res.data.length == 6) this.check = JSON.parse(res.data[5]);
          else this.check = false;
        })
        .catch(err => {

        });
    },

    likeOrUnlinkeMaxim(value){
      switch (value) {
        case false:
          axios.post('/unlike_maxim/'+this.maxim[1])
            .then(res => {
              if(res.status == 200) {
                this.maxim[5] = false;
                // this.maxim[3]--;
              }
            })
            .catch(err => {
              //
            });
          break;
        case true:
          axios.post('/like_maxim/'+this.maxim[1])
            .then(res => {
              if(res.status == 200) {
                this.maxim[5] = true;
                // this.maxim[3]++;
              }
            })
            .catch(err => {
              //
            });
          break;

      }
    },
    cardModal() {
      if(this.$store.getters.user.id){
        this.$modal.open({
            parent: this,
            component: this.modal,
            hasModalCard: true
        })
      }else{
        this.$refs.notAuthAlert.style.display = 'block';
      }
    },
  },

  watch: {
    check(value,old_val){
      if(old_val != undefined && this.$store.getters.user.id > 0){
        if(value == false) {
          this.likeOrUnlinkeMaxim(value);
        }else{
          this.likeOrUnlinkeMaxim(value);
        }
      // unauthenticated
      }else if(old_val != undefined){
        this.$refs.notAuthAlert.style.display = 'block';
        this.check = false;
      }
    }
  }

});


Vue.component('latest-commented-posts',{
  template: `
  <div class="latest-commented-posts">
    <h3>{{ $store.getters.lang.latest_commented_posts }}</h3>
    <ul>
      <li class="p-5" v-for="item in data">
        <h4>{{ customize(item.question.title) }}</h4>
        <p >{{ customize(JSON.parse(item.topic).ops[0].insert) }}</p>
      </li>
    </ul>
  </div>
  `,

  mounted(){
    this.getLatestCommentedPosts();
  },

  data(){
    return {
      data: [],
    }
  },

  methods: {
    getLatestCommentedPosts(){
      axios.get('/get_latest_commented_posts/'+this.$route.params.id)
      .then(res => this.data = res.data)
      .catch(err => {
        //
      });
    },

    customize(val){
      if(val.length>50) return val.slice(0,50)+'...';
      else return val+"...";
    }
  }
});
