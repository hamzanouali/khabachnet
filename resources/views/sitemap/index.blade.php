<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

	<url>
		<loc>http://khabach.net/login#/ar</loc>
		<changefreq>yearly</changefreq>
	</url>

	<url>
		<loc>http://khabach.net/register#/ar</loc>
		<changefreq>yearly</changefreq>
	</url>

	<url>
		<loc>http://khabach.net/password/reset#/ar</loc>
		<changefreq>yearly</changefreq>
	</url>

	<url>
		<loc>http://khabach.net/#/ar</loc>
		<changefreq>always</changefreq>
	</url>

	<url>
		<loc>http://khabach.net/#/ar/search=</loc>
		<changefreq>yearly</changefreq>
	</url>

	<url>
		<loc>http://khabach.net/#/ar/search=</loc>
		<changefreq>yearly</changefreq>
	</url>

  @foreach ($questions as $question)
      <url>
          <loc>https://khabach.net/#/ar/{{ $question->id }}/{{ preg_replace('/[\s\/]/','-',$question->title) }}</loc>
          <lastmod>{{ date('y-m-d', strtotime($question->updated_at)) }}</lastmod>
          <changefreq>weekly</changefreq>
          <priority>0.6</priority>
      </url>
  @endforeach
	
</urlset>
