@component('mail::message')
# {{ __('messages.reset_password_header') }}


{{ __('messages.reset_password_body') }}

@component('mail::button', ['url' => $url])
{{ __('messages.reset_password_button') }}
@endcomponent
<br>
{{ $url }}
<br>
{{ __('messages.reset_password_regards') }}
<br>
{{ config('app.name') }}
@endcomponent
