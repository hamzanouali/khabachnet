<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content=" منصة لإيجاد حل للمشاكل التقنية و مشاركة الأفكار الهادفة ">
        <meta name="keywords" content="مشاكل،برمجة،حلول،مشاركة،أفكار،programming,skills,resolved">
        <meta name="author" content="Hamza Nouali">
        <link rel="icon" sizes="24x24" type="image/png" href="/images/logo/khabach-ico.png">
        <?php if(isset(explode('/',$_SERVER['REQUEST_URI'])[4])) { ?>
          <title> {!! explode('/',$_SERVER['REQUEST_URI'])[4] !!} </title>
        <?php } else { ?>
          <title> {!! config('app.name') !!} </title>
        <?php } ?>
        <!-- <link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/2.4.0/firebase-ui-auth.css" /> -->
        <!-- <link rel="stylesheet" href="~buefy/lib/buefy.css"> -->
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="">

      <div id="app">
        <router-view></router-view>
        <!-- footer -->
        <div class="container-fuild">
        <br><br><br>
          <div style="width:100px;margin:auto;">
            <img src="/images/logo/logo_khabach.png">
          </div>
        <br>
          <div class="footer">
            <p>جميع الحقوق محفوظة لموقع خباش | khabach.com</p>
          </div>
        </div>
        <!-- end footer -->
      </div>
      <div id="end"></div>
      <script src="/js/app.home.js"></script>
    </body>
</html>
