@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" style="background-color: #efefef;" id="login">

    <div>
      <a href="/#/">
        <h3 style="text-align:center;"> عودة للصفحة الرئيسية </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column loginDiv" >

        <form class="form-horizontal text-center" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1> تسجيل الدخول </h1>

          @if (session('status'))
            <b-message type="is-success" class="nextCaseWarning" dir="rtl">
                {{ session('status') }}
            </b-message>
          @endif

          <div>
            <div class="field form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div class="control">
                <input id="email" type="email" placeholder="البريد الإلكتروني " class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder=" كلمة السر " class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field">
                <button class="myloginBTN button is-primary is-large">
                  دخول
                </button>
            </div>
            <!-- forgot password -->
            <p class="is-centered"><a href="{{ route('password.request') }}"> نسيت كلمة السر </a></p>
            <hr>

          </div>
          <p class="is-centered"><a href="{{ route('register') }}"> إنشاء حساب </a></p>
        </form>
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>

<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection

<script>


// class Error {
//
//   constructor(){
//     this.error = {};
//   }
//
//   get(field){
//     if(this.error[field]){
//       // if(this.error[field][0] && this.error[field][1]) return this.error[field];
//       // else
//       return this.error[field][0];
//     }
//   }
//
//   record(error){
//     this.error = error
//   }
//
// }
//
// new Vue({
//   el:'#login',
//   update(){
//
//     NProgress.done();
//
//     // if(firebase == undefined) apiHelpers.getFirebase(this.$refs);
//
//     // this will close the modal wich contain a message :
//     // ( the page that you did request require authentication )
//     // setTimeout(()=>{
//     //   this.$store.commit('pageRequireAuth',false);
//     // },3000);
//
//     this.firebaseGoogleLogin();
//   },
//
//   mounted(){
//
//     NProgress.done();
//
//     apiHelpers.getFirebase(this.$refs);
//
//     // this will close the modal wich contain a message :
//     // ( the page that you did request require authentication )
//     setTimeout(()=>{
//       this.$store.commit('pageRequireAuth',false);
//     },3000);
//
//     this.firebaseGoogleLogin();
//
//   },
//   data() {
//     return {
//       email : '',
//       password : '',
//       err_obj : new Error(),
//       subClass : 'myloginBTN button is-primary is-large',
//       fb_class : 'button is-info is-large facebookBTN radius-0 is-loading',
//       google_class : 'button is-large googleBTN radius-0 is-loading',
//       social_login : false,
//       loginWithSocialMedia : false,
//       is_a_firebase_user: false,
//       social_info_is_ready: false,
//     }
//   },
//   methods: {
//     switchLoginToEmail(){
//       this.email = '';
//       this.password = '';
//       this.subClass = 'myloginBTN button is-primary is-large';
//       this.fb_class = 'button is-info is-large facebookBTN radius-0 is-loading';
//       this.google_class = 'button is-large googleBTN radius-0 is-loading';
//       this.social_login = false;
//       this.loginWithSocialMedia = false;
//       this.is_a_firebase_user= false;
//     },
//     manualLogin(object){
//
//       console.log('login..');
//
//       // preparing credentials
//       let user_object = {
//         email : this.email,
//         password : this.password
//       }
//
//       // preparing request url
//       let request = '/login';
//
//       // if we pass an object as parameter
//       if(object && object.id){
//         // customizing user info object
//         user_object = object;
//         user_object.password = object.id+object.email
//         user_object.password_confirmation = object.password
//         // end here
//         request = '/register';
//       }else{
//         this.subClass = 'myloginBTN button is-primary is-large is-loading';
//       }
//
//       user_object.remember = 1;
//       // send the request
//       axios.post(request,user_object)
//
//       .then(response => {
//         if(response.status == 200){
//           // get the authenticated user
//           axios.get('/isAuth')
//           .then(res => {
//             if(res.status == 200) {
//               window.location.href = '/#'+this.$store.getters.last_route;
//             }
//           });
//         }
//       })
//
//       .catch(error => {
//
//         this.err_obj.record(error.response.data);
//
//         // if the user is using firebase api
//         if(this.is_a_firebase_user && error.response.data.email && this.err_obj.get('message') == 'البريد الإلكتروني الذي أدخلته غير موجود.'){
//           this.email = object.email;
//           this.password = object.id+object.email;
//           this.manualLogin();
//         }
//         //
//         this.subClass = 'myloginBTN button is-primary is-large';
//         // remove alert message
//         // setTimeout(n => {
//         //   this.err_obj.record({
//         //     error : {}
//         //   });
//         // },5000);
//
//       })
//
//     },
//
//     firebaseGoogleLogin(){
//       firebase.auth().onAuthStateChanged((user) => {
//         if(user) {
//           // console.log(user);
//           // return '';
//           // User is signed in.
//           // this.is_a_firebase_user = true;
//           // var object = {
//           //   // id : user.uid,
//           //   name : user.displayName,
//           //   email : user.email
//           // }
//           this.email = user.email;
//           this.social_info_is_ready = true;
//           // this.loginWithSocialMedia = true;
//           // this.manualLogin(object);
//
//         }
//       });
//     }
//
//   }
// });


</script>
