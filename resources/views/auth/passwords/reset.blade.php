@extends('layouts.app')

@section('content')
<div class="container-fluid" id="login">
<br><br><br>
  <div>
    <a href="/#/">
      <h3 style="text-align:center;"> عودة للصفحة الرئيسية </h3>
    </a>
    <br />
    <br />
  </div>

  <div class="columns">

      <div class="column is-4">
      </div>

      <div class="column loginDiv" >

      <form v-if="!show_forgot_password" method="POST" action="{{ route('password.request') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">
        <h1> تغيير كلمة السر </h1>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div>
          <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="control">
              <input type="email" placeholder=" البريد الإلكتروني " id="email" name="email" value="{{ $email or old('email') }}" required autofocus>

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="field {{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="control">
              <input type="password" placeholder=" كلمة السر "name="password" required>

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="field {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <div class="control">
              <input id="password-confirm" type="password" placeholder="تأكيد كلمة السر "name="password_confirmation" required>

              @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="field">
              <button class="myloginBTN button is-primary is-large">
                دخول
              </button>
          </div>
          <p class="is-centered"><a @click="show_forgot_password = true"> نسيت كلمة السر </a></p>
          <hr>

        </div>
        <p class="is-centered"><router-link to="/register"> ليس لدي حساب </router-link></p>
        <a href="https://icons8.com/android-icons" target="_blank">Icons download from Icons8</a>
      </form>
      </div>

      <div class="column is-4">
      </div>
  </div>
</div>
<!--
<div class="container">
    <div class="row">
      <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p>
        <div class="is-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

-->
@endsection
