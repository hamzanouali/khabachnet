@extends('layouts.app')
@section('content')
<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="container-fluid" id="login">

  <br><br><br>
  <div>
    <a href="/#/">
      <h3 style="text-align:center;"> عودة للصفحة الرئيسية </h3>
    </a>
    <br />
    <br />
  </div>
  <div class="columns">

      <div class="column is-4">
      </div>

      <div class="column loginDiv" >

      <form method="POST" action="{{ route('password.email') }}" style="padding:10px 20px;">
          {{ csrf_field() }}
        <h1> إستعادة كلمة السر </h1>

        @if (session('status'))
        <b-message type="is-success" class="nextCaseWarning" dir="rtl">
            {{ session('status') }}
        </b-message>
        @endif
        <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="control">
            <input type="email" name="email" value="{{ old('email') }}" id="email" placeholder=" البريد الإلكتروني " required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="field">
            <button class="myloginBTN button is-primary is-large">
              إرسال
            </button>
        </div>
      </form>

      </div>

      <div class="column is-4">
      </div>
  </div>
</div>
@endsection
