@extends('layouts.app')

@section('content')
<br><br><br><br>
  <div class="container-fluid" style="background-color: #efefef;" id="login">

    <div>
      <a href="/#/">
        <h3 style="text-align:center;"> عودة للصفحة الرئيسية </h3>
      </a>
      <br />
      <br />
    </div>


    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column loginDiv" >

        <form class="form-horizontal text-center" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1> إنشاء حساب </h1>

          <div>
            <div class="field form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <div class="control">
                <input id="name" type="text" placeholder=" إسم المستخدم " class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div class="control">
                <input id="email" type="email" placeholder="البريد الإلكتروني " class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder=" كلمة السر " class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password_confirmation" type="password" placeholder=" كلمة السر " class="form-control" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="field">
                <button class="myloginBTN button is-primary is-large">
                  دخول
                </button>
            </div>
            <!-- forgot password -->
            <p class="is-centered"><a href="{{ route('password.request') }}"> نسيت كلمة السر </a></p>
            <hr>

          </div>
          <p class="is-centered"><a href="{{ route('login') }}"> لديك حساب بالفعل ؟ </a></p>
        </form>
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>


<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection
