<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => ' كلمة السر يجب أن لا  تقل عن 6 أحرف و أن تتطابق مع التأكيد. ',
    'reset' => ' كلمة السر الخاصة بك تم استرجاعها! ',
    'sent' => ' قمنا بإرسال  رابط استرجاع حسابك إلى بريدك الإلكتروني! ',
    'token' => ' رمز التفعيل الذي أرسلته غير صالح ',
    'user' => " لم نجد أي  مستخدم طبقا لهذه البيانات! ",

];
