<?php

return [

  'reset_password_body' => ' بعد الضغط على الزر سيتم تحويلك إلى الصفحة التي تسمح لك بإعادة تعيين كلمة سر جديدة لحسابك، الرابط
  يصبح غير صالح بعد مرور ساعة من وصول هذه الرسالة إليك ',
  'reset_password_header' => ' عملية إستعادة الحساب ',
  'reset_password_button' => ' إعادة تعيين كلمة السر ',
  'reset_password_regards' => ' منصة خباش تتمنى لك يوما طيبًا ',
  'account_activation_header' => ' تفعيل الحساب ',
  'account_activation_body' => ' قم بالضغط على الزر أو على الرابط في الأسفل لإكمال تفعيل حسابك ',
  'account_activation_button' => ' تفعيل الحساب ',

];

?>
