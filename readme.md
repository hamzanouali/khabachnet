## Built with:
- Laravel
- Vue.js
- Bulma
- jQuery

## Features:
- Infinite comment system
- Notification system
- CRUD 
- Filter
- Search
- Emails
- Dashboard
- and more features.....